import argparse
import csv
import logging.config
import os
from io import StringIO
from pathlib import Path

import paramiko
import requests

from logger_config import init_logger

PROCESSED_ORDER_ID_TXT = 'last_processed_order_id.txt'

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='Tool Import-Do-Restaurace.')
    parser.add_argument('--csv-url', type=str, required=True, help='URL of location of orders.csv file. Must be accessible from internet without auth.')
    parser.add_argument('--sftp-host', type=str, required=True, help='Target SFTP host (port assumed :22).')
    parser.add_argument('--sftp-username', type=str, required=True, help='Target SFTP username.')
    parser.add_argument('--sftp-password', type=str, required=True, help='Target SFTP password.')

    args = parser.parse_args()

    csv_url = args.csv_url
    sftp_host = args.sftp_host
    sftp_username = args.sftp_username
    sftp_password = args.sftp_password

    logger.info(f'Downloading {csv_url}')
    csv_response = requests.get(csv_url)
    csv_response.raise_for_status()

    reader = csv.DictReader(StringIO(csv_response.content.decode('utf-8-sig')), delimiter=",", quotechar='"')
    orders = []
    active_order = []
    for row in reader:
        if row:
            if active_order and active_order[0]['id'] != row['id']:
                orders.append(active_order)
                active_order = []

            active_order.append(row)

    if active_order:
        orders.append(active_order)

    logger.info(f"Found a total of {len(orders):,} orders.")
    logger.info(f"Connecting to SFTP {sftp_username}@{sftp_host}.")

    with paramiko.SSHClient() as ssh:
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(sftp_host, port=22, username=sftp_username, password=sftp_password)
        sftp = ssh.open_sftp()
        logger.info("SFTP Connection OK")

        last_processed_order_id = None
        skipped = 0
        processed = 0
        errors = 0

        if os.path.exists(PROCESSED_ORDER_ID_TXT):
            last_processed_order_id = int(Path(PROCESSED_ORDER_ID_TXT).read_text())

        for order in orders:
            shipping = None
            billing = None
            parts = []
            for part in order:
                if part['orderItemCode'].startswith('SHIPPING'):
                    if shipping is not None:
                        logger.error(u"Multiple SHIPPING rows! %s", part)
                    shipping = part
                    continue

                if part['orderItemCode'].startswith('BILLING'):
                    if billing is not None:
                        logger.error(u"Multiple BILLING rows! %s", part)
                    billing = part
                    continue

                parts.append(part)

            if shipping is None:
                logger.error(u"No SHIPPING rows! %s")
            if billing is None:
                logger.error(u"No BILLING rows! %s")
            if not parts:
                logger.error(u"No PARTS rows! %s")

            order_id = int(shipping['id'])
            logger.debug(f"Processing order {order_id}")
            logger.debug(f"Found {len(parts)} parts in order {order_id}.")

            if last_processed_order_id is not None and last_processed_order_id >= order_id:
                logger.debug(f"Skipping order as it is older than last processed {last_processed_order_id}")
                skipped += 1
                continue

            if shipping['orderItemCode'] == 'SHIPPING4':
                shop = 'SHOP_PP'
                dir = 'pp'
            elif shipping['orderItemCode'] == 'SHIPPING67':
                shop = 'SHOP_LM'
                dir = 'lm'
            else:
                logger.error(f"Unknown {shipping['orderItemCode']}, ignoring whole order {order_id}")
                errors += 1
                continue

            header = [
                '',  # Id
                shop,  # KodStola
                '',  # FirmaId
                f"{shipping['deliveryFullName']} - {shipping['phone']}",  # Poznamka
                100,  # KodVybavujucejOsoby
                '',  # IDprihlasenehouzivatela
                '',  # HUcetID
                shipping['deliveryFullName'],  # FirmaNazov1
                shipping['deliveryCompany'],  # FirmaNazov2
                shipping['deliveryStreetWithHouseNumber'],  # FirmaUlica
                shipping['deliveryCity'],  # FirmaObec
                shipping['deliveryZipWithoutWhitespaces'],  # FirmaPsc
                shipping['phone'],  # FirmaMobil
                shipping['email'],  # FirmaEmail
                '',  # PlatbaHotovost
                '',  # PlatbaKarta
                1 if shipping['paid'] == '1' else 0,  # Zaplaceno
            ]

            items = []
            for part in parts:
                items.append([
                    part['orderItemCode'],  # PLU_tovarove
                    part['orderItemAmount'],  # Pocet
                    '',
                    '',  # KoefPocet
                    '',  # KoefCena
                    part['orderItemUnitPriceWithVat'],  # CenaMjSDph
                ])

            items.append([
                1667,
                '',
                shipping['poznamka'],  # poznamka
                '',
                '',
                '',
            ])

            file_path = f'{dir}/suctyutf8{order_id}.txt'
            logger.info(f"Storing {len(items)} rows to {file_path} (as utf8)")

            with sftp.file(file_path, 'w') as f:
                writer = csv.writer(f, delimiter=';', quoting=csv.QUOTE_MINIMAL)

                writer.writerow(header)
                for row in items:
                    writer.writerow(row)

            processed += 1
            logger.info(f"Finished order {order_id} OK!")
            Path(PROCESSED_ORDER_ID_TXT).write_text(str(order_id))

        logger.info(f"All done, skipped {skipped:,d} orders, processed {processed:,d} orders, errors: {errors:,d}.")

    logger.info("SFTP connection closed.")


if __name__ == "__main__":
    init_logger(global_level=logging.INFO)
    main()
