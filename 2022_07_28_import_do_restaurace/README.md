# Import do Restaurace

## Description

Script that downloads a CSV file from internet, processes orders in it (in a VERY strange manner) and generates a couple of TXT files based on it. Connects
to a SFTP server and stores those TXT files in there.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python idr.py --csv-url=https://515618.myshoptet.com/export/orders.csv?patternId=10&hash=e4051c0af2eb0bb663e971898b20862620c62a0d44ff33fbf8c075d04d1e7838 --sftp-host=xxx.host --sftp-username=yyy --sftp-password=zzz
```

All four parameters will be provided by Zarety.
