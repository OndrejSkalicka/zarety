import argparse
import datetime
import logging.config
import re
from typing import Tuple

import requests
from bs4 import BeautifulSoup

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='Automatic myshoptet uploader.')
    parser.add_argument('--shop-url', type=str, required=True, help='URL of admin, should end with .../admin/ .')
    parser.add_argument('--username', type=str, required=True, help='username.')
    parser.add_argument('--password', type=str, required=True, help='password.')
    parser.add_argument('--threshold-minutes', type=int, help='order age threshold in minutes.', default=5)

    args = parser.parse_args()

    shop_url = args.shop_url
    username = args.username
    password = args.password
    threshold_minutes = args.threshold_minutes

    masked_password = password[0] + ((len(password) - 2) * '*') + password[-1]

    admin_url = shop_url
    product_import_url = f'{shop_url}import-produktu/'
    login_url = f'{shop_url}login/'

    logger.info(f"Input parameters {shop_url}, {username}, {masked_password}, threshold {threshold_minutes}m")

    # start session with shoptet
    s = requests.Session()
    logger.info('Session started')

    # grab login page
    login_page = s.get(admin_url, allow_redirects=True)
    login_page.raise_for_status()

    # fetch csrf
    _, csrf = grab_title_csrf(login_page.text)

    logger.info(f'Current cookie jar: {s.cookies}')

    # log in
    login_result = s.post(
        url=login_url,
        data={
            'action': 'login',
            'email': username,
            'password': password,
            '__csrf__': csrf,
            'buttonAction': 'undefined'
        },
        allow_redirects=True,
    )
    login_result.raise_for_status()

    logger.info(f'Logged in via {login_url}: {login_result}.')

    _, csrf = grab_title_csrf(login_result.text)

    # https://503448.myshoptet.com/admin/prehled-objednavek/-1/?order=date&from=1&type=asc
    orders_url = f'{shop_url}prehled-objednavek/-1/?order=date&from=1&type=asc'

    orders_result = s.get(url=orders_url)
    orders_result.raise_for_status()
    title, csrf = grab_title_csrf(orders_result.text)

    logger.info(f'Found title {title} / csrf {csrf}')

    bs = BeautifulSoup(orders_result.text, 'html.parser')

    form = bs.find(id='orders-listing')
    all_form_data = {
        'buttonAction': 'saveAndStay',
    }
    for input_tag in form.find_all('input'):
        if 'name' not in input_tag.attrs:
            continue

        if input_tag.attrs['type'] == 'checkbox':
            continue

        all_form_data[input_tag.attrs['name']] = input_tag.attrs.get('value', '')

    for select_tag in form.find_all('select'):
        selected = select_tag.find_all('option', selected=True)
        if selected:
            all_form_data[select_tag.attrs['name']] = selected[0].attrs['value']

    all_form_data_dirty = False

    table = form.find('table')
    if table is None:
        logger.info("No orders")
        return

    for tr in table.find('tbody').find_all('tr'):
        tds = tr.find_all('td')
        date_str = tds[1].find_all(text=True)[-1].strip()
        order_code = tds[1].find(class_='item-code').text.strip()
        date_parsed = datetime.datetime.strptime(date_str, '%d.%m.%Y %H:%M')
        age = datetime.datetime.now() - date_parsed
        logger.info(f"Found an order ID {order_code} with timestamp {date_str} (parsed {date_parsed}). Age {age.seconds}s.")

        if age.seconds > threshold_minutes * 60:
            logger.info(f'Order {order_code} too old for this shit. Moving to In Progress.')
            all_form_data[tds[5].find('select').attrs['name']] = '-2'
            all_form_data_dirty = True

    if not all_form_data_dirty:
        logger.info("No orders to modify.")
        return

    logger.info("Data are dirty, submit. Form data: {}", all_form_data)
    all_form_data['__csrf__'] = grab_csrf(orders_result.text)

    update_result = s.post(
        url=orders_url,
        data=all_form_data,
        allow_redirects=True
    )
    update_result.raise_for_status()

    bs = BeautifulSoup(update_result.text, 'html.parser')

    errors = [x.text for x in bs.findAll('div', {'class': 'systemMessage--error'})]
    if errors:
        logger.error(f"Found errors in response! {errors}")
        raise Exception
    success_messages = ';'.join(x.text.strip() for x in bs.findAll('div', {'class': 'systemMessage--success'}))
    logger.info(f'Success messages: {success_messages}')


def grab_title_csrf(text: str) -> Tuple[str, str]:
    title = grab_title(text)
    csrf = grab_csrf(text)

    logger.debug(f'Extracted CSRF {csrf} from page "{title}"')

    return title, csrf


def grab_title(text: str) -> str:
    return BeautifulSoup(text, 'html.parser').find('title').text


def grab_csrf(text: str) -> str:
    return re.findall(r'shoptet.csrfToken = "(.*?)"', text)[0]


if __name__ == "__main__":
    init_logger(logging.DEBUG)
    main()
