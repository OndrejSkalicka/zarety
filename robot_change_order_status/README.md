# Order-status-change Robot

## Description

Script that logs into administration, checks orders in 'not processed' status and all those older than X minutes moves to 'processing'.

## Usage

Requires Python 3.8+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python robot_change_order_status.py --shop-url=https://503448.myshoptet.com/admin/ --username=jiridrazny@gmail.com --password=XXXX
```

All parameters should be provided by Zarety, same as frequency of calls. This script does not produce any output apart from logs.
