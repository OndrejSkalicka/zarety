import logging

import sys
from logging.handlers import RotatingFileHandler

logger = logging.getLogger(__name__)


# See https://stackoverflow.com/a/56944256/600169 and https://en.wikipedia.org/wiki/ANSI_escape_code
class ColorFormatter(logging.Formatter):
    grey = "\033[37;20m"
    black = "\033[30;20m"
    yellow = "\033[33;20m"
    red = "\033[31;20m"
    bold_red = "\033[31;1m"
    reset = "\033[0m"
    format_str = "%(asctime)s - %(levelname)-8s %(filename)-20s - %(message)s "

    FORMATS = {
        logging.DEBUG: grey + format_str + reset,
        logging.INFO: format_str,
        logging.WARNING: yellow + format_str + reset,
        logging.ERROR: red + format_str + reset,
        logging.CRITICAL: bold_red + format_str + reset
    }

    def format(self, record: logging.LogRecord) -> str:
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt, datefmt='%Y-%m-%dT%H:%M:%S%z')
        return formatter.format(record)


def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))


def init_logger(main_level: int = logging.DEBUG, global_level: int = logging.DEBUG) -> None:
    console_handler = logging.StreamHandler(stream=sys.stdout)
    console_handler.setLevel(main_level)
    console_handler.setFormatter(ColorFormatter())

    file_handler = RotatingFileHandler('log.txt', mode='a', maxBytes=10000000, backupCount=10, encoding='utf-8')

    logging.getLogger('').addHandler(console_handler)
    logging.getLogger('').addHandler(file_handler)
    logging.getLogger('').setLevel(global_level)
    logging.getLogger('__main__').setLevel(main_level)

    sys.excepthook = handle_exception
