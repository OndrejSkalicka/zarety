# Elinkx downloader

## Description

Script that downloads XML from Elinkx, finds URL of export, and downloads+saves that file.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python elinkx.py "--xml-url=https://private-ws-sk.elinkx.biz/service.asmx/getProductCatalogueFullDownloadXML?login=XXX&password=YYY&onStock=true&Comodities=" --download-path=out.xml
```
