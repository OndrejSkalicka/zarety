import argparse
import logging.config
import xml.etree.ElementTree as ET

import requests

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='Downloads a file pointed to via a getProductCatalogue')
    parser.add_argument('--xml-url', type=str, required=True, help='URL of the xml catalogue')
    parser.add_argument('--download-path', type=str, required=True, help='Where to store result')

    args = parser.parse_args()

    xml_url = args.xml_url
    download_path = args.download_path

    logger.info(f"Downloading {xml_url}")

    xml_response = requests.get(xml_url)
    xml_response.raise_for_status()

    logger.info("Parsing XML")

    tree = ET.fromstring(xml_response.text)

    target_url = tree.find('./{http://www.elinkx.cz}ProductListStatus/{http://www.elinkx.cz}url').text

    logger.info(f"Extracted target url {target_url}, downloading")

    target_response = requests.get(target_url)
    target_response.raise_for_status()

    logger.info(f"Saving to {download_path}")

    with open(download_path, 'wb') as f:
        f.write(target_response.content)

    logger.info(f"Written {len(target_response.content):,}B to {download_path}")


if __name__ == "__main__":
    init_logger(global_level=logging.DEBUG, main_level=logging.INFO)
    main()
