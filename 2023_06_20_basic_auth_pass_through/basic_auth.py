import argparse
import logging.config

import requests
from requests.auth import HTTPBasicAuth

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='Basic Auth pass-through.')
    parser.add_argument('--url', type=str, required=True, help='URL to download.')
    parser.add_argument('--username', type=str, required=True, help='Basic auth username')
    parser.add_argument('--password', type=str, required=True, help='Basic auth password')
    parser.add_argument('--output-path', type=str, required=True, help='Path on filesystem where to store the result.')

    args = parser.parse_args()

    url = args.url
    username = args.username
    password = args.password
    output_path = args.output_path

    logger.info(f"Downloading {url}")

    response = requests.get(url, auth=HTTPBasicAuth(username, password))
    response.raise_for_status()

    with open(output_path, 'wb') as fh:
        fh.write(response.content)

    logger.info(f"Saved {len(response.content):,} bytes to {output_path}")


if __name__ == "__main__":
    init_logger(global_level=logging.INFO)
    main()
