# Basic Auth pass-through

## Description

Script that takes a URL and basic auth parameters, fetches contents of the URL and stores in a file.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python basic_auth.py --url=https://www.promolog.com/en/api/stock --username=buka --password=ku1234 --output-path=result.xml
```
