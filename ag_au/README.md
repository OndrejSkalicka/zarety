# AU/AG updater

## Description

Script that downloads an excel from internet, fetches prices of gold and silver, and calculates prices in source excel. Result is stored to filesystem.

## Usage

Requires Python 3.8+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python agau.py --source-xlsx-url=https://foo.com/download/products.xls --metalpriceapi-api-token=65xxx68 --output-xlsx-path=agau.xlsx
```

First two parameters should be provided by Zarety (what to download and an API key for paid commodities-api.com). Third one if where to store result and
can be any relative or absolute path (needs write permissions of course).
