import argparse
import logging.config
from io import BytesIO
from typing import Iterable, Optional, Sequence

import requests
import xlsxwriter
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.cell import Cell
from openpyxl.cell.cell import TYPE_STRING, TYPE_NULL
from openpyxl.worksheet.worksheet import Worksheet

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='AGAU, the Gold and Silver golem.')
    parser.add_argument('--source-xlsx-url', type=str, required=True, help='URL input/source xlsx file.')
    parser.add_argument('--metalpriceapi-api-token', type=str, required=True, help='Token to metalpriceapi.com.')
    parser.add_argument('--output-xlsx-path', type=str, required=True, help='Path on local filesystem where to store results.')
    parser.add_argument('--currency', type=str, default='EUR', help='Currency of results. Default: EUR.')

    args = parser.parse_args()

    source_file_url = args.source_xlsx_url
    metalpriceapi_api_token = args.metalpriceapi_api_token
    output_file = args.output_xlsx_path
    currency = args.currency

    au_ag_in_currency_resp = requests.get(f'https://api.metalpriceapi.com/v1/latest?api_key={metalpriceapi_api_token}&base={currency}&currencies=XAU,XAG')
    au_ag_in_currency_resp.raise_for_status()

    json = au_ag_in_currency_resp.json()

    if not json['success']:
        raise Exception(f'Request to fetch prices failed: {json}')

    au_in_currency = 1.0 / json['rates']['XAU']
    ag_in_currency = 1.0 / json['rates']['XAG']

    logger.info("Fetched prices from metalpriceapi.")

    logger.info(f"AU price {au_in_currency} {currency}.")
    logger.info(f"AG price {ag_in_currency} {currency}.")

    file_response = requests.get(source_file_url, stream=True)
    file_response.raise_for_status()
    logger.info(f'Downloaded file {source_file_url}, response {file_response}, length {len(file_response.raw.data)}B.')

    wb_src: Workbook = load_workbook(filename=BytesIO(file_response.raw.data))
    ws_src: Worksheet = wb_src.worksheets[0]

    wb_new = xlsxwriter.Workbook(output_file)
    ws_new = wb_new.add_worksheet(ws_src.title)

    row: Sequence[Cell]

    header: Iterable[Cell] = next(ws_src.rows)
    name_idx: Optional[int] = None
    sn_idx: Optional[int] = None
    plu_idx: Optional[int] = None

    header_row = []
    for idx, c in enumerate(header):
        header_row.append(cell_to_scalar(c))
        if c.value == 'name':
            name_idx = idx

        if c.value == 'serialNumber':
            sn_idx = idx

        if c.value == 'plu':
            plu_idx = idx

    header_row.append('price')
    header_row.append('filteringProperty:Vykupna-cena')
    ws_new.write_row(0, 0, header_row)

    assert name_idx is not None, 'Column "name" not in source sheet.'
    assert sn_idx is not None, 'Column "serialNumber" not in source sheet.'
    assert plu_idx is not None, 'Column "plu" not in source sheet.'

    cnt = 0
    row_num = 1
    for row in list(ws_src.rows)[1:]:
        assert len(row) == len(header_row) - 2
        name = row[name_idx]
        sn = as_float(row[sn_idx])
        plu = as_float(row[plu_idx])

        new_row = list(cell_to_scalar(c) for c in row)
        sn_value = None
        plu_value = None

        if is_au(name):
            assert not is_ag(name), "Both 'zlato' and 'striebro' in single row."

            if sn is not None:
                sn_value = round(sn * au_in_currency, 2)

            if plu is not None:
                plu_value = round(plu * au_in_currency, 2)

        if is_ag(name):
            assert not is_au(name), "Both 'zlato' and 'striebro' in single row."

            if sn is not None:
                sn_value = round(sn * ag_in_currency, 2)

            if plu is not None:
                plu_value = round(plu * ag_in_currency, 2)

        new_row.append(sn_value)
        new_row.append(plu_value)

        ws_new.write_row(row_num, 0, new_row)
        row_num += 1

        cnt += 1
        if cnt % 50 == 0:
            logger.debug("Processed %d rows", cnt)

    logger.debug("Processed %d rows", cnt)

    wb_new.close()
    logger.info(f"Saved to {output_file}.")


def cell_to_scalar(cell: Cell) -> str:
    if cell.data_type == TYPE_STRING:
        return str(cell.value)

    if cell.data_type == TYPE_NULL:
        return ''

    raise ValueError("Illegal cell type %s" % cell.data_type)


def is_au(cell: Cell) -> bool:
    if cell.data_type != TYPE_STRING:
        return False

    return 'zlato' in str(cell.value).lower()


def is_ag(cell: Cell) -> bool:
    if cell.data_type != TYPE_STRING:
        return False

    return 'striebro' in str(cell.value).lower() or 'stribro' in str(cell.value).lower()


def as_float(cell: Cell) -> Optional[float]:
    if cell.data_type == TYPE_NULL:
        return None

    if cell.data_type != TYPE_STRING:
        logger.warning(f"Wrong data type for cell {cell}: {cell.data_type}")
        return None

    if cell.value == '':
        return None

    return float(cell.value.replace(',', '.'))


if __name__ == "__main__":
    init_logger(logging.DEBUG)
    main()
