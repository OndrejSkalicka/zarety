import argparse
import csv
import logging.config
from io import StringIO
from typing import Any

import paramiko
import requests

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='Stock Reset(ter).')
    parser.add_argument('--csv-url', type=str, required=True, help='URL of CSV to download.')
    parser.add_argument('-v', '--verbose', default=False, action='store_true', help='Enables more verbose logging (debug).')
    parser.add_argument('--sftp-host', type=str, required=True, help='Target SFTP host (port assumed :22).')
    parser.add_argument('--sftp-username', type=str, required=True, help='Target SFTP username.')
    parser.add_argument('--sftp-password', type=str, required=True, help='Target SFTP password.')
    parser.add_argument('--sftp-path', type=str, required=True, help='Path on SFTP where to store CSV result.')

    args = parser.parse_args()

    csv_url = args.csv_url
    sftp_host = args.sftp_host
    sftp_username = args.sftp_username
    sftp_password = args.sftp_password
    sftp_path = args.sftp_path

    if not args.verbose:
        logging.getLogger('').setLevel(logging.INFO)
        logging.getLogger('__main__').setLevel(logging.INFO)

    logger.info(f"Downloading {csv_url}")

    response = requests.get(csv_url)
    response.raise_for_status()
    response.encoding = 'cp1250'  # because fuck you...
    csv_text = response.text
    csv_io = StringIO(csv_text)

    reader = csv.reader(csv_io, delimiter=';')

    header = next(reader)
    header_map = {v: idx for idx, v in enumerate(header)}

    for col in ['resetFlagActive', 'stock:reset', 'code', 'pairCode']:
        if col not in header_map:
            raise Exception(f"Column '{col} not found in source data")

    new_data = []

    row: list[Any]
    for row in reader:
        label = f"code='{row[header_map['code']]}' // pairCode='{row[header_map['pairCode']]}'"

        logger.debug(f"[{label}] Processing row")

        if int(row[header_map['resetFlagActive']]) == 1:
            logger.debug(f'[{label}] Product has resetFlagActive=1, skipping')
            continue

        # stock:reset at -1, set back to zero, update flag
        if float(row[header_map['stock:reset']]) == -1.0:
            logger.info(f"[{label}] Product has stock:reset=-1, setting back to stock:reset=0, resetFlagActive=1")
            row[header_map['stock:reset']] = 0
            row[header_map['resetFlagActive']] = 1
            new_data.append(row)
            continue

        # full zeroes
        all_zeroes = True
        col: str
        for col, idx in header_map.items():
            if col.startswith('stock:') and float(row[idx]) != 0.0:
                logger.debug(f'[{label} Product has non-zero stock at {col}: {row[idx]}, Setting initial resetFlagActive=1')
                all_zeroes = False
                break

        if all_zeroes:
            logger.info(f'[{label}] Product has zeroes in all stocks, setting stock:reset=-1')
            row[header_map['stock:reset']] = -1
            new_data.append(row)
            continue

        row[header_map['resetFlagActive']] = 1
        new_data.append(row)

    logger.info(f"Found {len(new_data)} products to update, connecting to ftp {sftp_host}:22")

    with paramiko.SSHClient() as ssh:
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(sftp_host, port=22, username=sftp_username, password=sftp_password)
        sftp = ssh.open_sftp()

        logger.info(f"Connected to {sftp_host}")

        with sftp.file(sftp_path, 'wb') as fh:
            output = StringIO()
            writer = csv.writer(output, delimiter=';')
            writer.writerow(header)
            writer.writerows(new_data)

            logger.info(f"Generated output CSV in memory, converting to cp1250 and storing to {sftp_path}")
            fh.write(output.getvalue().encode('cp1250'))

    logger.info(f"Saved result to {sftp_path}")


if __name__ == "__main__":
    init_logger(global_level=logging.INFO)
    main()
