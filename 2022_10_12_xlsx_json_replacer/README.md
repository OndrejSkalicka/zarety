# XLSX JSON Replacer

## Description

Script that takes an input JSON (from url) and XLSX (from filesystem), does replacements in the JSON, and stores result to filesystem.3

Only `"sku": "some-string"` fields in JSON are affected. Replacement replaces first column of XLSX with second one. First row is header and is ignored.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python --json-url=https://www.damijashop.cz/user/documents/novy_json_upr.json --xlsx-path=Zmena_cisel_kodu.xlsx --output-json-path=out.json
```
