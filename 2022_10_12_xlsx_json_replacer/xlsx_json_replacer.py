import argparse
import json
import logging.config
from dataclasses import dataclass

import requests
from openpyxl import load_workbook, Workbook
from openpyxl.worksheet.worksheet import Worksheet

from logger_config import init_logger

PROCESSED_ORDER_ID_TXT = 'last_processed_order_id.txt'

logger = logging.getLogger(__name__)


@dataclass
class Statistics:
    sku_found: int = 0
    replacements: int = 0


def main():
    parser = argparse.ArgumentParser(description='CSV to JSON Processor.')
    parser.add_argument('--json-url', type=str, required=True, help='URL of JSON to download.')
    parser.add_argument('--xlsx-path', type=str, required=True, help='Path on filesystem where XLSX with transformations is stored.')
    parser.add_argument('--output-json-path', type=str, required=True, help='Path on filesystem where to store results.')

    args = parser.parse_args()

    json_url = args.json_url
    xlsx_path = args.xlsx_path
    output_json_path = args.output_json_path

    logger.info(f"Downloading {json_url}")
    response = requests.get(json_url)
    response.raise_for_status()
    response.encoding = 'utf-8'

    json_text = response.text
    json_data = json.loads(json_text)

    logger.info(f"Loading XLSX {xlsx_path}")
    wb: Workbook = load_workbook(filename=xlsx_path)
    ws: Worksheet = wb.worksheets[0]

    transformations: dict[str, str] = {}
    for row in list(ws.rows)[1:]:
        if len(row) < 2:
            continue

        transformation_from = str(row[0].value)
        transformation_to = str(row[1].value)

        if transformation_from in transformations:
            logger.warning(f"Duplicate transformation {transformation_from} at {row[0].coordinate}.")

        transformations[transformation_from] = transformation_to

    logger.info(f"Loaded {len(transformations)} transformations from XLSX.")
    wb.close()

    stats = Statistics()
    replace_recur(json_data, transformations, stats)

    logger.info(f"Transformation statistics: {stats}")

    with open(f'{output_json_path}', 'w') as f:
        f.write(json.dumps(json_data, indent='  '))

    logger.info(f"Result written to {output_json_path}")


def replace_recur(data, transformations: dict[str, str], stats: Statistics):
    if isinstance(data, dict):
        if 'sku' in data:
            stats.sku_found += 1
            value = data['sku']

            if type(value) != str:
                logger.warning(f"SKU of non-string-value: {value}")
                value = str(value)

            if value in transformations:
                value = transformations[value]
                stats.replacements += 1

            data['sku'] = value

        for value in data.values():
            replace_recur(value, transformations, stats)

    elif isinstance(data, list):
        for value in data:
            replace_recur(value, transformations, stats)


if __name__ == "__main__":
    init_logger(global_level=logging.INFO, main_level=logging.INFO)
    main()
