# HappyZoo Reverse Engineering

## Description

Reverse engineered python script for HappyZoo. No idea what it does.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python happyzoo.py "--orders-csv-url=https://www.happyzoo.cz/export/orders.csv?XXX" "--category-csv-url=https://www.happyzoo.cz/export/products.csv?XXX" --output-csv-path=out.csv
```

Note that dateFrom and dateUntil in Orders CSV URL will be replaced with current-date and current-date-minus-365-days.
