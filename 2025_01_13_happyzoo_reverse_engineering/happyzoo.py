import argparse
import datetime
import io
import logging.config
import re
from collections import Counter

import pandas as pd
import requests
import urllib3
import urllib3.exceptions

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='Emulates a reverse-engineered script we got without any documentation ... ')
    parser.add_argument('--orders-csv-url', type=str, required=True, help='URL of the CSV export of orders')
    parser.add_argument('--category-csv-url', type=str, required=True, help='URL of the CSV export of category feed')
    parser.add_argument('--output-csv-path', type=str, required=True, help='Path on local filesystem where to store results.')

    args = parser.parse_args()

    orders_csv_url = args.orders_csv_url
    category_csv_url = args.category_csv_url
    output_csv_path = args.output_csv_path

    time_start = datetime.datetime.now()

    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)  # https://stackoverflow.com/a/28002687

    date_from = (datetime.datetime.today() - datetime.timedelta(days=365)).strftime("%Y-%m-%d")
    date_until = datetime.datetime.today().strftime("%Y-%m-%d")

    orders_csv_url = re.sub(r'[?&]dateFrom=[^&]+', '', orders_csv_url)
    orders_csv_url = re.sub(r'[?&]dateUntil=[^&]+', '', orders_csv_url)
    orders_csv_url += '&dateFrom=' + date_from
    orders_csv_url += '&dateUntil=' + date_until

    logger.info(f'Updated Orders CSV URL with date_from={date_from} and date_until={date_until}: {orders_csv_url}')

    logger.info(f"Downloading {orders_csv_url}")
    csv_response = requests.get(orders_csv_url, verify=False)
    csv_response.raise_for_status()
    orders_csv = csv_response.content
    logger.info(f'Downloaded {round(len(orders_csv) / 1024 / 1024, 2):,} MB')

    logger.info(f"Downloading {category_csv_url}")
    csv_response = requests.get(category_csv_url, verify=False)
    csv_response.raise_for_status()
    category_csv = csv_response.content
    logger.info(f'Downloaded {round(len(category_csv) / 1024 / 1024, 2):,} MB')

    try:
        df_product_category = pd.read_csv(io.StringIO(category_csv.decode('utf-8')), sep=";",
                                          dtype={"code": str, "pairCode": str, "defaultCategory": str, "availabilityInStock": str})
    except UnicodeDecodeError:
        logger.warning("Fallback to windows-1250 encoding")
        df_product_category = pd.read_csv(io.StringIO(category_csv.decode('windows-1250')), sep=";",
                                          dtype={"code": str, "pairCode": str, "defaultCategory": str, "availabilityInStock": str})

    df_product_category["category"] = df_product_category.defaultCategory.str.split(" > ", expand=True)[1]

    df_product_category["Skladem Total"] = df_product_category.filter(regex="stock:").sum(axis=1)
    df_product_category.loc[df_product_category["Skladem Total"] < 1, "availabilityInStock"] = 0
    df_product_category = df_product_category[["code", "pairCode", "category", "availabilityInStock"]]
    logger.info("Dataframe product_category - succcess")

    # Prepare orders dataframe
    try:
        df_orders = pd.read_csv(io.StringIO(orders_csv.decode('utf-8')), sep=";", usecols=["code", "orderItemCode"])
    except UnicodeDecodeError:
        print("Fallback to windows-1250 encoding")
        df_orders = pd.read_csv(io.StringIO(orders_csv.decode('windows-1250')), sep=";", usecols=["code", "orderItemCode"])
    df_orders = df_orders.dropna()  # drop nan rows
    df_orders = df_orders[~df_orders.orderItemCode.str.contains("SHIPPING|BILLING|COUPON")]  # removes billing, shipping and coupon rows
    df_orders.columns = ["orderId", "code"]
    logger.info("Dataframe orders - succcess")

    # Prepare products sales df
    df_products = df_orders.groupby(["code"]).agg(unique_purchases=('code', 'count')).reset_index()
    logger.info("Dataframe products - succcess")

    # Get Bestsellers in category
    df_bestsellers_in_category = pd.merge(df_product_category, df_products, on="code", how="left")
    df_bestsellers_in_category = df_bestsellers_in_category[
        df_bestsellers_in_category["availabilityInStock"] == "Skladem"]  # Removes items which aren't in stock
    df_bestsellers_in_category = df_bestsellers_in_category.sort_values(["category", "unique_purchases"], ascending=[True, False]).groupby("category").head(10)
    df_bestsellers_in_category = df_bestsellers_in_category.groupby("category")["code"].apply(list).reset_index()
    df_bestsellers_in_category.columns = ["category", "categoryBestsellers"]
    logger.info("Dataframe bestsellers_in_category - succcess")

    # Get Related Products
    in_stock_products = df_product_category[
        df_product_category["availabilityInStock"] == "Skladem"].code.to_list()  # Create list of available (instock) products
    df_products['relatedProducts'] = df_products["code"].apply(get_related, dataframe=df_orders, in_stock_products=in_stock_products)
    df_products = df_products[["code", "relatedProducts"]]
    logger.info("Dataframe related products - succcess")

    # Prepare final dataframe with all info
    df_final = pd.merge(df_product_category, df_bestsellers_in_category, on="category", how="left")
    df_final = pd.merge(df_final, df_products, on="code", how="left")
    # This fill NaN values with [] where products are missing in product_category CSV file
    df_final["relatedProducts"] = df_final["relatedProducts"].fillna("").apply(list)
    # This fill NaN values with [] where products are missing in product_category CSV file
    df_final["categoryBestsellers"] = df_final["categoryBestsellers"].fillna("").apply(list)
    df_final["recommendations"] = df_final["relatedProducts"] + df_final["categoryBestsellers"]
    df_final['recommendations'] = df_final['recommendations'].apply(lambda x: x[:5])
    logger.info("Dataframe final - success")

    # Suppress warnings, see https://stackoverflow.com/a/53954986
    pd.options.mode.chained_assignment = None
    # Final transformation and csv export
    df_export = df_final[["code", "pairCode"]]
    export_columns = ["relatedProduct", "relatedProduct2", "relatedProduct3", "relatedProduct4", "relatedProduct5"]
    df_export[export_columns] = pd.DataFrame(df_final['recommendations'].to_list(), columns=export_columns)
    with open(output_csv_path, 'w') as fh:
        df_export.to_csv(fh, sep=";", index=False)

    logger.info("CSV export - success")

    time_stop = datetime.datetime.now()
    logger.info(f"Script finnished: {time_stop}")
    logger.info(f"Total execution time: {str(time_stop - time_start)} seconds")

    logger.info('Finished successfully')


# function for pandas apply method
def get_related(product, dataframe, in_stock_products):
    orderlist = dataframe[dataframe.code == product].orderId.to_list()
    related_products = dataframe[
        (dataframe["orderId"].isin(orderlist)) & (dataframe["code"] != product) & (dataframe["code"].isin(in_stock_products))].code.to_list()
    c = Counter(related_products)

    result = []

    for _ in c.most_common(5):
        result.append(_[0])

    return result


if __name__ == "__main__":
    init_logger(global_level=logging.DEBUG, main_level=logging.INFO)
    main()
