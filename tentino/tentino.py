import csv
import logging.config
import re
from collections import OrderedDict
from datetime import datetime, timedelta
from xml.dom.minidom import getDOMImplementation, Document, Element

import requests
from bs4 import BeautifulSoup

import xmlserialize
from logger_config import init_logger

logger = logging.getLogger(__name__)

STARS_MAP = {
    'width: 100%;': 5,
    'width: 80%;': 4,
    'width: 60%;': 3,
    'width: 40%;': 2,
    'width: 20%;': 1,
}

if __name__ == "__main__":
    init_logger(logging.DEBUG)

    urls = []

    with open('url_pro_scrape_recenzi_v5_8.csv', newline='', mode='r', encoding='utf-8-sig') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            if row:
                urls.append(row[0])

    result = []

    rating_id = 42
    processed = 0
    price_missing = 0
    http_errors = {}
    skips = {
        'price_missing': 0,
        'order_id_missing': 0,
    }

    for url in urls:
        processed += 1
        response = requests.get(url)

        if not response.ok:
            logger.warning(f'Skipping {url} as it had {response.status_code} code')

            if response.status_code not in http_errors:
                http_errors[response.status_code] = 0
            http_errors[response.status_code] += 1
            continue

        response.raise_for_status()
        logger.info(f"HTTP OK for url {url}")
        soup = BeautifulSoup(response.text, 'html.parser')

        item = OrderedDict()

        item['product_name'] = soup.find('h1', class_='c716 c1335').text
        item['url'] = url

        price_str = soup.find('span', class_='c2009').text.strip().replace('\xa0', '').replace(',', '.').replace('Kč', '')
        if price_str == '':
            logger.warning(f'Skipping {url} as it does not have price')
            skips['price_missing'] += 1
            continue

        price = float(price_str)

        item['price'] = price

        order_id_div = soup.find('span', class_='c767')

        if order_id_div is None:
            skips['order_id_missing'] += 1
            continue

        item['order_id'] = order_id_div.text.strip()
        ean_div = soup.find('div', class_='c1992')
        item['ean'] = ean_div.find('span', class_='c767').text.strip() if ean_div is not None else ''
        item['reviews'] = []

        rating_url = soup.find('div', id='detail-anchor-product-rating').attrs['data-route']
        logger.debug(f'rating url {rating_url}')

        response = requests.get(f'https://www.tentino.cz{rating_url}&isShowAll=1')
        response.raise_for_status()
        rating_soup = BeautifulSoup(response.text, 'html.parser')

        for rating_div in rating_soup.find('div', class_='c1818').find_all('div', class_='product-rating__comment'):
            if 'admin' in rating_div.attrs['class']:
                logger.debug('Ignoring comment from admin')
                continue
            review = OrderedDict()

            review['rating_id'] = rating_id
            rating_id += 1

            date_text = rating_div.find('span', class_='c1758').text.replace('-', '').strip()
            review['unix_timestamp'] = int((datetime.strptime(date_text, '%d.%m.%Y') + timedelta(hours=12)).timestamp())

            review['rating'] = STARS_MAP[str(rating_div.find('div', class_='c1761').attrs['style'])]
            review['recommends'] = 1
            review['summary'] = rating_div.find('div', class_='c1762').text.strip()
            review['rating_name'] = re.sub(r' {2,}', ' ', rating_div.find('span', class_='c1757').text.replace('\n', ' ')).strip()

            item['reviews'].append(review)

        result.append(item)

        logger.info(f'Processed {processed}/{len(urls)} {url}')

    logger.info(f"Processed a total of {processed:,}, skipped {skips} due to missing elements and {http_errors} due to HTTP errors.")

    with open('result.xml', 'wb') as xmlfile:
        impl = getDOMImplementation()

        xml_doc: Document = getDOMImplementation().createDocument(None, "products", None)
        e_products: Element = xml_doc.documentElement

        for product in result:
            e_product: Element = xml_doc.createElement('product')
            e_products.appendChild(e_product)

            for k, v in product.items():
                if k != 'reviews':
                    el: Element = xml_doc.createElement(k)

                    if str(v):
                        te: Element = xml_doc.createTextNode(str(v))
                        el.appendChild(te)
                    e_product.appendChild(el)

                else:
                    el_reviews: Element = xml_doc.createElement('reviews')
                    e_product.appendChild(el_reviews)

                    for review in v:
                        el_review: Element = xml_doc.createElement('review')
                        el_reviews.appendChild(el_review)
                        for review_k, review_v in review.items():
                            el: Element = xml_doc.createElement(review_k)
                            te: Element = xml_doc.createTextNode(str(review_v))
                            el.appendChild(te)
                            el_review.appendChild(el)

        xmlfile.write(xml_doc.toprettyxml(encoding='utf-8'))
    logger.info("Written to result.xml")

    with open('result-dtx.xml', 'wb') as xmlfile:
        xmlfile.write(xmlserialize.serialize(result, 'products'))
    logger.info("Written to result-dtx.xml")
