from collections.abc import Iterable
from dataclasses import dataclass, field
from typing import Dict, List, Any
from xml.dom.minidom import getDOMImplementation, Document, Element


@dataclass
class ListWithName:
    name: str
    items: List = field(default_factory=list)


def serialize(data: List[Dict], root_element_wrap: str) -> bytes:
    if not isinstance(data, Iterable) and not isinstance(data, ListWithName):
        raise ValueError(f'Requires iterable / ListWithName, got {type(data)}')

    xml_doc: Document = getDOMImplementation().createDocument(None, root_element_wrap, None)

    _add_node(xml_doc, xml_doc.documentElement, data)

    return xml_doc.toprettyxml(encoding='utf-8')


def _add_list_as_nodes(xml_doc: Document, target: Element, wrapper_name: str, values: Iterable) -> None:
    for value in values:
        element: Element = xml_doc.createElement(wrapper_name)
        _add_node(xml_doc, element, value)

        target.appendChild(element)


def _add_node(xml_doc: Document, target: Element, value: Any) -> None:
    if isinstance(value, dict):
        for k, v in value.items():
            element: Element = xml_doc.createElement(k)
            target.appendChild(element)
            _add_node(xml_doc, element, v)

        return

    if isinstance(value, ListWithName):
        _add_list_as_nodes(xml_doc, target, value.name, value.items)
        return

    if isinstance(value, str):
        if not value:
            return
        text_node: Element = xml_doc.createTextNode(str(value))
        target.appendChild(text_node)
        return

    if isinstance(value, int) or isinstance(value, float):
        text_node: Element = xml_doc.createTextNode(str(value))
        target.appendChild(text_node)
        return

    if isinstance(value, Iterable):
        if not target.tagName.endswith('s'):
            raise ValueError(f'Cannot auto-determine child-tag-name from {target.tagName}')

        _add_list_as_nodes(xml_doc, target, target.tagName[:-1], value)

        return

    raise ValueError(f'Illegal value type {type(value)}')


if __name__ == "__main__":
    data = [
        {
            "product_name": "AKCE! Bistro stůl koktejlový ⌀80 cm + ubrus ZDARMA",
            "url": "https://www.tentino.cz/AKCE-Bistro-stul-koktejlovy-80-cm-ubrus-ZDARMA-d666.htm",
            "price": 1753.9,
            "order_id": "130 AKCE",
            'text': ListWithName('tt', items=[
                1, 2, 3
            ]),
            "ean": "",
            "reviews": [
                {
                    "rating_id": 42,
                    "unix_timestamp": 1657620000,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "",
                    "rating_name": "Julius Š."
                },
                {
                    "rating_id": 43,
                    "unix_timestamp": 1656583200,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "",
                    "rating_name": "Petr M."
                },
                {
                    "rating_id": 44,
                    "unix_timestamp": 1653559200,
                    "rating": 1,
                    "recommends": 1,
                    "summary": "Stoly nam byli dorucene bez potahu. Bohuzel recepcni pri prevzeti stoly nezkontrolovala, proto vyrobek jsme nemohli reklamovat. Samotne stoly jsou v poradku.",
                    "rating_name": "Sofiia T."
                },
                {
                    "rating_id": 45,
                    "unix_timestamp": 1652608800,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "Za nás palec nahoru",
                    "rating_name": "Petr N."
                },
                {
                    "rating_id": 46,
                    "unix_timestamp": 1651140000,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "",
                    "rating_name": "Ondřej M."
                },
                {
                    "rating_id": 47,
                    "unix_timestamp": 1650880800,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "",
                    "rating_name": "Josef G."
                },
                {
                    "rating_id": 48,
                    "unix_timestamp": 1633860000,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "Ideální, melmi skladný a v dobrém poměru  výkon cena..",
                    "rating_name": "Patrick P."
                },
                {
                    "rating_id": 49,
                    "unix_timestamp": 1632391200,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "",
                    "rating_name": "Jiří H."
                },
                {
                    "rating_id": 50,
                    "unix_timestamp": 1632218400,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "",
                    "rating_name": "Hana P."
                },
                {
                    "rating_id": 51,
                    "unix_timestamp": 1632045600,
                    "rating": 3,
                    "recommends": 1,
                    "summary": "Konstrukčně super pevný. Ale stul ma povrch který po prvním použití bez ubrusu má stopy po cokolade a červeného vína které nelze umýt..  takže 3 pracovni dny a estetický vypadá hrozně..",
                    "rating_name": "Katerina S."
                },
                {
                    "rating_id": 52,
                    "unix_timestamp": 1622714400,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "Dobrá kvalita",
                    "rating_name": "Jiří P."
                },
                {
                    "rating_id": 53,
                    "unix_timestamp": 1622023200,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "",
                    "rating_name": "Nikola P."
                },
                {
                    "rating_id": 54,
                    "unix_timestamp": 1621591200,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "Bistro stůl je pevný, stabilní a velký. Prostál jsem u neho a prodebatoval s přáteli celý večer až do brzkého rána:)",
                    "rating_name": "Pavel Z."
                },
                {
                    "rating_id": 55,
                    "unix_timestamp": 1621332000,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "Výborná kvalita zboží  za dobrou cenu a dopravou  až do domu zdarma,doporučuji",
                    "rating_name": "Bronislava J."
                },
                {
                    "rating_id": 56,
                    "unix_timestamp": 1620122400,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "",
                    "rating_name": "Iva T."
                },
                {
                    "rating_id": 57,
                    "unix_timestamp": 1619344800,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "Subtilní a pěkný party stolek",
                    "rating_name": "Emil Ž."
                },
                {
                    "rating_id": 58,
                    "unix_timestamp": 1619172000,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "Praktické, fajn cena i vizáž :))",
                    "rating_name": "Jana Z."
                },
                {
                    "rating_id": 59,
                    "unix_timestamp": 1618999200,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "Dobré ceny, rychlé dodání a informováni o stavu objednávky.",
                    "rating_name": "Petr V."
                }
            ]
        },
        {
            "product_name": "Automatický dezinfekční stojan - BÍLÝ",
            "url": "https://www.tentino.cz/Automaticky-dezinfekcni-stojan-BILY-d663_1048528676.htm",
            "price": 4804.0,
            "order_id": "Z-1",
            "ean": "",
            "reviews": []
        },
        {
            "product_name": "Automatický dezinfekční stojan - ČERNÝ",
            "url": "https://www.tentino.cz/Automaticky-dezinfekcni-stojan-CERNY-d663_1048528677.htm",
            "price": 4804.0,
            "order_id": "Z-1",
            "ean": "",
            "reviews": []
        },
        {
            "product_name": "Automatický dezinfekční stojan - ŠEDÝ",
            "url": "https://www.tentino.cz/Automaticky-dezinfekcni-stojan-SEDY-d663_1048528678.htm",
            "price": 4804.0,
            "order_id": "Z-1",
            "ean": "",
            "reviews": []
        },
        {
            "product_name": "Automatický dezinfekční stojan - více barev",
            "url": "https://www.tentino.cz/Automaticky-dezinfekcni-stojan-vice-barev-d663.htm",
            "price": 4804.0,
            "order_id": "Z-1",
            "ean": "",
            "reviews": []
        },
        {
            "product_name": "Banner na suchý zip 2m",
            "url": "https://www.tentino.cz/banner-na-suchy-zip-2m",
            "price": 2436.0,
            "order_id": "SZ-2",
            "ean": "",
            "reviews": []
        },
        {
            "product_name": "Banner na suchý zip 3 m",
            "url": "https://www.tentino.cz/Banner-na-suchy-zip-3-m-d197.htm",
            "price": 2938.0,
            "order_id": "SZ-3",
            "ean": "",
            "reviews": [
                {
                    "rating_id": 60,
                    "unix_timestamp": 1623837600,
                    "rating": 5,
                    "recommends": 1,
                    "summary": "",
                    "rating_name": "Filip B."
                }
            ]
        },
        {
            "product_name": "Banner na suchý zip 4,5 m",
            "url": "https://www.tentino.cz/Banner-na-suchy-zip-4-5-m-d198.htm",
            "price": 3471.0,
            "order_id": "SZ-4,5",
            "ean": "",
            "reviews": []
        },
        {
            "product_name": "Banner na suchý zip 4 m",
            "url": "https://www.tentino.cz/Banner-na-suchy-zip-4-m-d199.htm",
            "price": 3223.0,
            "order_id": "SZ-4",
            "ean": "",
            "reviews": []
        },
        {
            "product_name": "Banner na suchý zip 5 m",
            "url": "https://www.tentino.cz/Banner-na-suchy-zip-5-m-d354.htm",
            "price": 3856.0,
            "order_id": "SZ-5",
            "ean": "",
            "reviews": []
        }
    ]
    print(serialize(
        data, 'products'
    ).decode('utf-8'))
