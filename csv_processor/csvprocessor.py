import argparse
import csv
import logging
import logging.config
import sys
from io import StringIO
from logging.handlers import RotatingFileHandler

import requests
import xlsxwriter

logger = logging.getLogger(__name__)


class ColorFormatter(logging.Formatter):
    grey = "\033[37;20m"
    yellow = "\033[33;20m"
    red = "\033[31;20m"
    bold_red = "\033[31;1m"
    reset = "\033[0m"
    format_str = "%(asctime)s.%(msecs)03d - %(levelname)-8s - %(message)s (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.DEBUG: grey + format_str + reset,
        logging.INFO: format_str,
        logging.WARNING: yellow + format_str + reset,
        logging.ERROR: red + format_str + reset,
        logging.CRITICAL: bold_red + format_str + reset
    }

    def format(self, record: logging.LogRecord) -> str:
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt, datefmt='%Y-%m-%dT%H:%M:%S%z')
        return formatter.format(record)


def init_logger(level: int = logging.DEBUG) -> None:
    console = logging.StreamHandler(stream=sys.stdout)
    console.setLevel(level)
    console.setFormatter(ColorFormatter())

    file = RotatingFileHandler('log.txt', 'a', 10000000, 10)
    file.setLevel(level)
    file.setFormatter(ColorFormatter())

    logging.getLogger('').addHandler(console)
    logging.getLogger('').addHandler(file)
    logging.getLogger('').setLevel(logging.WARN)
    logger.setLevel(level)


def main():
    parser = argparse.ArgumentParser(description='CSV Processor.')
    parser.add_argument('--csv-url', type=str, required=True, help='URL of CSV to download.')
    parser.add_argument('--target', type=str, required=True, help='Target file where to store result.')

    args = parser.parse_args()

    csv_url = args.csv_url
    target = args.target

    logger.info(f"Downloading {csv_url}")
    response = requests.get(csv_url)
    response.raise_for_status()
    response.encoding = 'cp1250'  # because fuck you...
    csv_text = response.text
    csv_io = StringIO(csv_text)

    reader = csv.reader(csv_io, delimiter=';')

    workbook = xlsxwriter.Workbook(target)
    sheet = workbook.add_worksheet('JA_TO_DELAT_NEBUDU')
    sheet.write_row(0, 0, ['code', 'pairCode', 'stock', 'availabilityOutOfStock'])

    row_num = 1
    first = True
    for row in reader:
        if first:
            first = False
            continue
        sheet.write_row(row_num, 0, [row[0], '', float(row[1]), row[2]])
        row_num += 1

        if row_num % 1_000 == 0:
            logger.info(f"Processed {row_num} rows")

    workbook.close()
    logger.info("Finished")


if __name__ == "__main__":
    init_logger(logging.INFO)
    main()
