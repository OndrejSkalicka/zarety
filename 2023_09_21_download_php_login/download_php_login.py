import argparse
import logging.config

import requests

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='Logins to a web, then downloads a file.')
    parser.add_argument('--login-url', type=str, required=True, help='URL of login page')
    parser.add_argument('--download-url', type=str, required=True, help='URL what to download')
    parser.add_argument('--username', type=str, required=True, help='Login username')
    parser.add_argument('--password', type=str, required=True, help='Login password')
    parser.add_argument('--download-path', type=str, required=True, help='Where to store result')

    args = parser.parse_args()

    login_url = args.login_url
    download_url = args.download_url
    username = args.username
    password = args.password
    download_path = args.download_path

    s = requests.Session()
    logger.info('Session started')

    response = s.post(login_url,
                      data={
                          'login': username,
                          'heslo': password,
                          'c_iobchod': 'BRW',
                          'akce': 'uzivatelLogin',
                          'submit': 'xxx',
                      })
    response.raise_for_status()

    logger.info(f"Logged in OK at {login_url}!")

    response = s.get(download_url)
    response.raise_for_status()
    response.encoding = 'utf-8'

    logger.info(f"Downloaded file {download_url}")

    with open(download_path, 'w', encoding='utf-8') as f:
        f.write(response.text)

    logger.info(f"Written {len(response.text)}B to {download_path}")


if __name__ == "__main__":
    init_logger(global_level=logging.DEBUG, main_level=logging.INFO)
    main()
