# PHP login + downloader

## Description

Script that logs into a PHP website and downloads a file to localhost.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python download_php_login.py "--login-url=https://velkoobchod.brwcz.cz/mod/login/LoginAkce.php?lang=cs" "--download-url=https://velkoobchod.brwcz.cz/cs/ostatni/export?akce=stahniXml" --username=XXX --password=YYY --download-path=out.xml
```
