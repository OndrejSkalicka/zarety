import argparse
import logging.config

from openpyxl import load_workbook, Workbook

from logger_config import init_logger

PROCESSED_ORDER_ID_TXT = 'last_processed_order_id.txt'

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='XLSX Column Joiner.')
    parser.add_argument('--input-xlsx', type=str, required=True, help='Path to input XLSX file.')
    parser.add_argument('--output-xlsx', type=str, required=True, help='Path to output XLSX file.')

    args = parser.parse_args()

    input_path = args.input_xlsx
    output_path = args.output_xlsx

    logger.info(f'Opening {input_path}')

    wb_s = load_workbook(filename=input_path)
    ws_s = wb_s.worksheets[0]

    wb_d = Workbook()
    ws_d = wb_d.active
    ws_d.title = f'Result - {ws_s.title}'

    header = None
    processed = 0
    ids_to_products = {}
    for row in list(ws_s.rows):
        if header is None:
            header = list(x.value for x in row)
            continue

        code = row[0].value
        data = list(x.value for x in row[1:])

        if code not in ids_to_products:
            ids_to_products[code] = []

        ids_to_products[code].append(data)
        processed += 1

        if processed % 1_000 == 0:
            logger.debug(f"Processed {processed:,} rows...")

    logger.info(f"Processed {processed:,} data rows into a set of {len(ids_to_products):,} unique keys.")

    max_product_size = 0
    for _, products in ids_to_products.items():
        max_product_size = max(max_product_size, len(products))

    logger.info(f'Highest number of products with the same key: {max_product_size:,}')

    new_header = [header[0]] + header[1:] * max_product_size
    ws_d.append(new_header)
    logger.info(f'New header: {new_header}')

    for product_id, products in ids_to_products.items():
        new_row = [product_id]

        for product in products:
            new_row += product

        ws_d.append(new_row)

    logger.info(f"Saved {len(ids_to_products):,} to sheet")

    wb_d.save(filename=output_path)
    logger.info(f"It's over, it's done. Saved to {output_path}")


if __name__ == "__main__":
    init_logger(global_level=logging.INFO)
    main()
