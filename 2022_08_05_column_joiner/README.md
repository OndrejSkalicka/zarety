# Column Joiner

## Description

Script that takes an excel file on input. Processes it, row by row, joining rows with same "first column" together into bigger (longer) rows.

Then, writes result to filesystem.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python column_joiner.py --input-xlsx=transformace-altu.xlsx --output-xlsx=out.xlsx
```
