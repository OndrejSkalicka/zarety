import argparse
import csv
import io
import logging.config
from io import StringIO
from sys import getsizeof

import paramiko
import requests

from logger_config import init_logger

logger = logging.getLogger(__name__)


def fs(size: int) -> str:
    if size < 1024:
        return f'{size}B'

    size = round(size / 1024, 1)

    if size < 1024:
        return f'{size}KB'

    size = round(size / 1024, 1)
    return f'{size}MB'


def main():
    parser = argparse.ArgumentParser(description='One shot FTP files renamer.')
    parser.add_argument('--csv-url', type=str, required=True, help='URL of CSV where to download input CSV')
    parser.add_argument('--sftp-host', type=str, required=True, help='Target SFTP host (port assumed :22).')
    parser.add_argument('--sftp-username', type=str, required=True, help='Target SFTP username.')
    parser.add_argument('--sftp-password', type=str, required=True, help='Target SFTP password.')
    parser.add_argument('--sftp-source-directory', type=str, required=True, help='Directory on SFTP where to read data.')
    parser.add_argument('--sftp-destination-directory', type=str, required=True, help='Directory on SFTP where to store results.')

    args = parser.parse_args()

    csv_url = args.csv_url
    sftp_host = args.sftp_host
    sftp_username = args.sftp_username
    sftp_password = args.sftp_password
    sftp_source_directory = args.sftp_source_directory
    sftp_destination_directory = args.sftp_destination_directory

    logger.info(f"Downloading CSV {csv_url}")
    response = requests.get(csv_url)
    response.raise_for_status()
    response.encoding = 'utf-8'
    csv_text = response.text
    csv_io = StringIO(csv_text)

    reader = csv.reader(csv_io, delimiter=',')
    next(reader)  # skip header
    items = list((str(row[0]), str(row[1])) for row in reader if row[0])

    missing_files = []

    with paramiko.SSHClient() as ssh, io.BytesIO() as memfile:
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(sftp_host, port=22, username=sftp_username, password=sftp_password)
        sftp = ssh.open_sftp()
        try:
            sftp.chdir(sftp_destination_directory)
        except IOError:
            sftp.mkdir(sftp_destination_directory)
        sftp.chdir('/')

        logger.info(f"Connected to {sftp_host}, source dir in {sftp_source_directory}, destination dir {sftp_destination_directory}")

        for idx, (from_name, to_name) in enumerate(items):
            memfile.seek(0)
            memfile.truncate(0)

            file_from = sftp_source_directory + '/' + from_name
            file_to = sftp_destination_directory + '/' + to_name

            logger.debug(f"[{idx + 1:7,} / {len(items):7,}] Trying to copy {file_from} to {file_to}")

            try:
                sftp.getfo(file_from, memfile)
                memfile.seek(0)
            except FileNotFoundError:
                logger.error(f"File {file_from} not found! Continue with next one.")
                missing_files.append(file_from)
                continue

            logger.debug(f'Downloaded {file_from}, size {getsizeof(memfile)}B')

            sftp.putfo(memfile, file_to)
            logger.info(f'[{idx + 1:7,} / {len(items):7,}] Copied {file_from} to {file_to} ({fs(getsizeof(memfile))})')

    if missing_files:
        logger.error(f"Could not process those files: {missing_files}")


if __name__ == "__main__":
    init_logger(global_level=logging.DEBUG, main_level=logging.INFO)
    main()
