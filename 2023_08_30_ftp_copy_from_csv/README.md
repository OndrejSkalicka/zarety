# SFTP renamer

## Description

Script that takes CSV from URL, downloads it, connects to SFTP and renames (or, rather, copies) files according to first two columns of the first sheet.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python ftp_copy_from_csv.py --sftp-host=ftp.myshoptet.com --sftp-username=ondra_563348 --sftp-password=XXX --sftp-source-directory=upload/produktove-foto --sftp-destination-directory=upload/produktove-foto-new "--csv-url=https://docs.google.com/spreadsheets/d/e/000?gid=000&single=true&output=csv"
```
