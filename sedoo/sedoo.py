import csv
import traceback
from datetime import datetime, timedelta
from time import time

import requests
from bs4 import BeautifulSoup
from openpyxl import Workbook
from requests_html import HTMLSession


def log(s):
    print('%s: %s' % (datetime.now(), s))


urls = []

with open('urls.csv', newline='', mode='r', encoding='utf-8-sig') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    for row in reader:
        if row:
            urls.append(row[0])

log('Loaded %d items from urls.csv' % len(urls))

result = []
errors = []

workbook = Workbook()
sheet_main = workbook.active
sheet_main.title = 'Sedoo'
sheet_urls = workbook.create_sheet('Sedoo URLs')

base_columns = ('URL', 'name', 'shortDescription', 'description', 'manufacturer', 'defaultImage', 'image',
                'categoryText', 'price', 'currency', 'availabilityInStock', 'downloads', 'dimensions', 'techSpecs')
for idx, title in enumerate(base_columns):
    sheet_main.cell(1, 1 + idx, title)

sheet_urls.cell(1, 1, 'URL')

session = HTMLSession()
xlsx_row = 2
surcharge_groups = {}
start_global = time()
averages = []
for idx, url in enumerate(urls):
    result_text = 'OK'
    start_item = time()
    try:
        log('Processing %s' % url)

        response = requests.get(url)
        response.raise_for_status()
        raw_soup = BeautifulSoup(response.text, 'html.parser')
        product_id = raw_soup.find('product-detail').attrs[':product_server']

        configurer_url = 'https://sedooz.sk/produktovy_konfigurator/api/option-group/%s/' % product_id
        response = requests.get(configurer_url)
        response.raise_for_status()

        config_json = response.json()

        for category in config_json:
            category_name = category['translations__name']

            if category_name not in surcharge_groups:
                index = len(base_columns) + len(surcharge_groups) + 1
                index_url = len(surcharge_groups) + 2
                surcharge_groups[category_name] = (index, index_url)
                sheet_main.cell(1, index, category_name)
                sheet_urls.cell(1, index_url, category_name)

            (index, index_url) = surcharge_groups[category_name]

            items_main = []
            for component in category['components']:
                for option in component['options']:
                    if category['widget_type'] == 2:
                        items_main.append('%s#%s=%.2f' % (
                            component['translations__display_name'],
                            option['translations__name'],
                            0.0 if option.get('price_incl_tax') is None else option.get('price_incl_tax')))
                    else:
                        items_main.append('%s=%.2f' % (
                            option['translations__name'],
                            0.0 if option.get('price_incl_tax') is None else option.get('price_incl_tax')))

            sheet_main.cell(xlsx_row, index, ";".join(items_main))

            items_url = []
            for component in category['components']:
                for option in component['options']:
                    items_url.append('%s;%s' % (option['translations__name'], option['image']['product_tail']))
            sheet_urls.cell(xlsx_row, index_url, "\n".join(items_url))

        r = session.get(url)

        r.html.render(timeout=120)
        r.html.html
        soup = BeautifulSoup(r.html.html, 'html.parser')

        # A url
        sheet_main.cell(xlsx_row, 1, url)
        # B name
        sheet_main.cell(xlsx_row, 2, soup.select_one('h1.product__title.product__title--detail').text)
        # C short-description
        sheet_main.cell(xlsx_row, 3,
                        ' > '.join(e.text for e in soup.select('div.product-main-panel div.product__category a')))
        # D description
        sheet_main.cell(xlsx_row, 4, soup.select_one('#popis-specifikacia div.product-description__content').text)
        # E manufacturer
        # F defaultImage =  G image
        sheet_main.cell(xlsx_row, 6, soup.select_one('#main-image').attrs['src'])
        sheet_main.cell(xlsx_row, 7, soup.select_one('#main-image').attrs['src'])
        # H category
        sheet_main.cell(xlsx_row, 8, ' > '.join(e.text.strip() for e in soup.select('.breadcrumbs a')))

        # I+J price + currency
        price = 'N/A'
        currency = 'N/A'

        price_block = soup.select_one('div.product__price.v-cloak--block')
        if price_block:
            (price_text, currency) = price_block.text.split(' ')
            price = float(price_text.replace(',', '.').replace('\xa0', ''))

        sheet_main.cell(xlsx_row, 9, price)
        sheet_main.cell(xlsx_row, 10, currency)

        # K availabilityInStock
        sheet_main.cell(xlsx_row, 11, soup.select_one('.product__stock ').text)

        # L downloads
        if soup.select('#downloads-tab a'):
            sheet_main.cell(xlsx_row, 12, "\n".join('%s;%s;%s' %
                                                    (a.attrs['href'],
                                                     a.select_one('.download-file__title').text,
                                                     a.select_one('.download-file__description').text)
                                                    for a in soup.select('#downloads-tab a')))

        # M dimensions
        if soup.select_one('#rozmery img'):
            sheet_main.cell(xlsx_row, 13, soup.select_one('#rozmery img').attrs['src'])

        # N techSpecs
        if soup.select_one('#technicka-specifikacia'):
            sheet_main.cell(xlsx_row, 14, soup.select_one('#technicka-specifikacia').text)

        sheet_urls.cell(xlsx_row, 1, url)

    except Exception as e:
        result_text = 'ERROR'
        traceback.print_exc()
        log('Error processing product %s: %s' % (url, e))
        errors.append((url, e))

    finally:
        xlsx_row += 1

        item_time = time() - start_item
        averages.append(item_time)
        time_per_item = ((time() - start_global) / (idx + 1))
        eta = (len(urls) - idx - 1) * time_per_item
        floating_average = sum(averages[-10:]) / len(averages[-10:])
        log("Processed % 4d/%d (%.1f%%) [%s], avg time per page: %.2fs, 10x floating average: %.2fs, ETA %ds (%s)" % (
            idx + 1, len(urls), (idx + 1) / len(urls) * 100.0, result_text, time_per_item, floating_average,
            eta, datetime.now() + timedelta(seconds=eta)
        ))

if errors:
    log("!!!!!!!!!!!!!!!!!!!!")
    log("There were %d errors" % len(errors))

    for url, error in errors:
        log('URL %s was not parsed: %s' % (url, error))

    log("!!!!!!!!!!!!!!!!!!!!")

workbook.save(filename='sedoo-%s.xlsx' % datetime.now().strftime('%Y-%d-%m-%H-%M-%S'))
