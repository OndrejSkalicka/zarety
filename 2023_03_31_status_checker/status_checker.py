import datetime
import time

import requests

while True:
    response = requests.get('https://www.seznam.cz/asdf')

    msg = '%s %d' % (datetime.datetime.now().isoformat(), response.status_code)
    with open('status.txt', 'a') as fh:
        fh.write(msg + "\n")
        print(msg)

    time.sleep(60 * 5)  # sleep 5 minutes
