from openpyxl import load_workbook, Workbook
from openpyxl.worksheet.worksheet import Worksheet

wb = load_workbook(filename='parametry.xlsx')
ws = wb['List1']  # type: Worksheet

items = {}

header = None
for row in ws.rows:
    if header is None:
        header = list(x.value for x in row)
        continue

    id = row[0].value
    if id not in items:
        items[id] = (
            id,
            row[1].value,
            []
        )

    items[id][2].append(row[2].value)

wb_d = Workbook()
target = wb_d.active
target.title = 'NemamRadZaretyho'

target.append(header)

for id, name, params in items.values():
    target.append([id, name] + params)
wb_d.save(filename='result.xlsx')
