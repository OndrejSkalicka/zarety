import argparse
import csv
import json
import logging.config
import time
from collections import defaultdict
from dataclasses import dataclass, field
from io import StringIO
from typing import Optional

import paramiko
import requests

from logger_config import init_logger

PROCESSED_ORDER_ID_TXT = 'last_processed_order_id.txt'

logger = logging.getLogger(__name__)


@dataclass
class Product:
    code: str
    friends: str
    nazev: str = field(repr=False)
    link: str = field(repr=False)
    cena: str = field(repr=False)
    fotka: str = field(repr=False)
    productname: str = field(repr=False)
    paircode: str
    description: str = field(repr=False)
    availabilityinstock: str = field(repr=False)
    stock: str = field(repr=False)
    shortdescription: str = field(repr=False)


def main():
    parser = argparse.ArgumentParser(description='CSV to JSON Processor.')
    parser.add_argument('--csv-url', type=str, required=True, help='URL of CSV to download.')
    parser.add_argument('--remove-from-url', action='append', help='String that will be removed from all URLs')
    parser.add_argument('--sftp-host', type=str, required=True, help='Target SFTP host (port assumed :22).')
    parser.add_argument('--sftp-username', type=str, required=True, help='Target SFTP username.')
    parser.add_argument('--sftp-password', type=str, required=True, help='Target SFTP password.')
    parser.add_argument('--sftp-directory', type=str, required=True, help='Directory on SFTP where to store results.')

    args = parser.parse_args()

    csv_url = args.csv_url
    remove_from_url = args.remove_from_url
    sftp_host = args.sftp_host
    sftp_username = args.sftp_username
    sftp_password = args.sftp_password
    sftp_directory = args.sftp_directory

    logger.info(f"Downloading {csv_url}")
    response = requests.get(csv_url)
    response.raise_for_status()
    response.encoding = 'utf-8'
    csv_text = response.text
    csv_io = StringIO(csv_text)

    reader = csv.reader(csv_io, delimiter=',')

    row = next(reader, None)
    assert row is not None
    field_names = list(c.strip() for c in row)

    products_by_code: dict[str, Product] = {}
    missing_friends: set[str] = set()
    all_products = []
    products_with_pair_code: dict[str, list[Product]] = defaultdict(list)

    for raw_row in reader:
        row = {field_names[idx]: col for idx, col in enumerate(raw_row)}

        product = Product(**row)
        all_products.append(product)
        if product.paircode:
            products_with_pair_code[product.paircode].append(product)

        assert row['code'] not in products_by_code, f"Row duplicity by {product.code}: {product}"
        products_by_code[row['code']] = product

    logger.info(f"Found {len(all_products):,} products.")

    with paramiko.SSHClient() as ssh:
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(sftp_host, port=22, username=sftp_username, password=sftp_password)
        sftp = ssh.open_sftp()
        try:
            sftp.chdir(sftp_directory)
        except IOError:
            sftp.mkdir(sftp_directory)
            sftp.chdir(sftp_directory)

        logger.info(f"Connected to {sftp_host}, working in {sftp_directory}")

        start_time = time.time()
        for idx, product in enumerate(all_products, start=1):
            result = {
                'name': product.nazev,
                'code': product.code,
                'url': try_strip_url(product.link, remove_from_url),
                'groups': [],
            }

            direct_friends: list[Product] = []
            for friend_code in product.friends.split(';') + [product.code]:
                if not friend_code:
                    continue

                if friend_code not in products_by_code:
                    missing_friends.add(friend_code)
                    continue

                direct_friends.append(products_by_code[friend_code])
            logger.debug(f'Direct friends of product {product}: {direct_friends}')

            all_friends = []
            for friend in direct_friends:
                if not friend.paircode:
                    logger.debug(f'Friend {friend} does not have paircode, not adding any more items.')
                    all_friends.append(friend)
                    continue

                paired_friends = products_with_pair_code[friend.paircode]
                all_friends += paired_friends
                logger.debug(f'Friend {friend} had {paired_friends} pair-code-friends, adding them all.')
            logger.debug(f'All friends: {all_friends}')

            groups_by_name = {}
            for friend in all_friends:
                if friend.nazev not in groups_by_name:
                    groups_by_name[friend.nazev] = {
                        'name': friend.nazev,
                        'description': friend.description,
                        'short_description': friend.shortdescription,
                        'products': []
                    }

                groups_by_name[friend.nazev]['products'].append({
                    'name': friend.productname,
                    'code': friend.code,
                    'url': try_strip_url(friend.link, remove_from_url),
                    'image': try_strip_url(friend.fotka, remove_from_url),
                    'price': friend.cena,
                    'stock': friend.stock,
                    'availability_in_stock': friend.availabilityinstock,
                })

            all_groups = sorted(groups_by_name.values(), key=lambda group: has_product_in_group(group, product.code), reverse=True)

            for group in all_groups:
                result['groups'].append(group)

            with sftp.file(f'{product.code}.json', 'w') as fh:
                fh.write(json.dumps(result, indent='  '))

            if idx % 100 == 0:
                logger.info(f"Processed {idx:,}/{len(all_products):,} items, active TPS: {idx / (time.time() - start_time):,.2f}.")

        logger.info(f"Processed {len(all_products):,}/{len(all_products):,} items.")


def has_product_in_group(group: dict, product_code: str) -> bool:
    for product in group['products']:
        if product['code'] == product_code:
            return True

    return False


def try_strip_url(url: str, remove_from_url: Optional[list[str]]) -> str:
    if not url:
        return url

    if not remove_from_url:
        return url

    for removal in remove_from_url:
        url = url.replace(removal, '')

    return url


if __name__ == "__main__":
    init_logger(global_level=logging.INFO, main_level=logging.INFO)
    main()
