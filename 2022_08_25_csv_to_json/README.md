# CSV to JSON

## Description

Script that takes an input CSV (from url), parses it and creates several JSONs out from it.

Then, uploads result to a sftp.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python csv_to_json.py --csv-url=https://feeds.mergado.com/mime-digital-shopalike-csv-cz-123.csv --remove-from-url=https://520881.myshoptet.com/ --remove-from-url=https://cdn.myshoptet.com/usr/520881.myshoptet.com --sftp-host=ftp.myxxx.com --sftp-username=xxx --sftp-password=xxx --sftp-directory=xxx
```

Note that the parameter `--remove-from-url=xxx` is repeatable.
