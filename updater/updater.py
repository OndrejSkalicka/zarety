import argparse
import logging.config
import re
from typing import Tuple

import requests
from bs4 import BeautifulSoup

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='Automatic myshoptet uploader.')
    parser.add_argument('--shop-url', type=str, required=True, help='URL of admin, should end with .../admin/ .')
    parser.add_argument('--username', type=str, required=True, help='username.')
    parser.add_argument('--password', type=str, required=True, help='password.')
    parser.add_argument('--file-url', type=str, required=True, help='url of file to upload.')
    parser.add_argument('--import-suffix', type=str, default='import-produktu/', help='suffix after admin/ to upload data to.')

    args = parser.parse_args()

    shop_url = args.shop_url
    username = args.username
    password = args.password
    file_url = args.file_url
    import_suffix = args.import_suffix

    masked_password = password[0] + ((len(password) - 2) * '*') + password[-1]

    admin_url = shop_url
    product_import_url = f'{shop_url}{import_suffix}'
    login_url = f'{shop_url}login/'

    logger.info(f"Input parameters {shop_url}, {username}, {masked_password}, {file_url}")

    # download file, do not allow for gzip as the response is directly forwarded to shoptet who goes crazy
    file_response = requests.get(file_url, stream=True, headers={'Accept-Encoding': 'identity'})
    file_response.raise_for_status()
    logger.info(f'Downloaded file {file_url}, response {file_response}, length {len(file_response.raw.data)}B.')

    # start session with shoptet
    s = requests.Session()
    logger.info('Session started')


    # grab login page
    login_page = s.get(admin_url, allow_redirects=True)
    login_page.raise_for_status()

    # fetch csrf
    _, csrf = grab_both(login_page.text)

    logger.info(f'Current cookie jar: {s.cookies}')

    # log in
    login_result = s.post(
        url=login_url,
        data={
            'action': 'login',
            'email': username,
            'password': password,
            '__csrf__': csrf
        },
        allow_redirects=True,
        headers={
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:123.0) Gecko/20100101 Firefox/123.0",
        }
    )
    login_result.raise_for_status()

    logger.info(f'Logged in via {login_url}: {login_result}.')
    logger.info('Page title after login: %s' % BeautifulSoup(login_result.text, 'html.parser').find('title'))

    _, csrf = grab_both(login_result.text)

    upload_result = s.post(
        url=product_import_url,
        files={
            'file': (str.rpartition(file_url, '/')[2], file_response.raw.data, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        },
        data={
            "__csrf__": csrf,
            "action": "import",
            "buttonAction": "csvImport",
            "importType": "2",
        },
        allow_redirects=True,
        headers={
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:123.0) Gecko/20100101 Firefox/123.0",
        }
    )
    upload_result.raise_for_status()

    logger.info(f'Uploaded file with result {upload_result}')
    logger.info('Page title after upload: %s' % BeautifulSoup(login_result.text, 'html.parser').find('title'))

    bs = BeautifulSoup(upload_result.text, 'html.parser')
    errors = [x.text for x in bs.findAll('div', {'class': 'systemMessage--error'})]
    if errors:
        logger.error(f"Found errors in response! {errors}")
        raise Exception

    success_messages = ';'.join(x.text.strip() for x in bs.findAll('div', {'class': 'systemMessage--success'}))

    logger.info(f'Success messages: {success_messages}')


def grab_both(text: str) -> Tuple[str, str]:
    title = grab_title(text)
    csrf = grab_csrf(text)

    logger.debug(f'Extracted CSRF {csrf} from page "{title}"')

    return title, csrf


def grab_title(text: str) -> str:
    return BeautifulSoup(text, 'html.parser').find('title').text


def grab_csrf(text: str) -> str:
    return re.findall(r'shoptet\.csrf\.token = "(.*?)"', text)[0]


if __name__ == "__main__":
    init_logger(logging.DEBUG)
    main()
