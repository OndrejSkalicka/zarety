# Import Nahradnich Dilu

## Description

Downloads a JSON export, processes all items in it from the RELATED_PRODUCTS section, and writes back into a CSV.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python import_nahradnich_dilu.py --json-url=https://feeds.mergado.com/mojerc-cz-plain-json-XXXXX.json --output-csv-path=out.csv
```
