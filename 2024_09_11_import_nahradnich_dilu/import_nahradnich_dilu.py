import argparse
import csv
import json
import logging.config
from collections import defaultdict
from io import StringIO

import requests

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='Parses a modifies an "Import Nahradnich Dilu" json file')
    parser.add_argument('--json-url', type=str, required=True, help='URL of the json to process')
    parser.add_argument('--output-csv-path', type=str, required=True, help='Path on filesystem where to store results.')

    args = parser.parse_args()

    json_url = args.json_url
    output_csv_path = args.output_csv_path

    json_response = requests.get(json_url)
    json_response.raise_for_status()
    json_response.encoding = 'utf-8'

    json_string = json_response.text

    products: list[dict] = json.loads(json_string)

    rpp_reverse = defaultdict(list)

    for product in products:
        codes = product.get('RELATED_PRODUCTS', {'CODE': []})['CODE']

        if isinstance(codes, str):
            codes = [codes]

        for related_product_code in codes:
            rpp_reverse[related_product_code].append((product['MANUFACTURER'], product['NAME']))

    logger.info(f"Processed {len(products):,} products.")

    max_width = max(len(x) for x in rpp_reverse.values())

    logger.info(f"Max width {max_width:,}")

    output = StringIO()
    writer = csv.writer(output, delimiter=';')
    writer.writerow(['CODE'] + list('CATEGORIES | CATEGORY {@@POSITION = {%d}}' % (d + 1) for d in range(max_width)))

    for k, v in sorted(rpp_reverse.items()):
        writer.writerow([k] + list(f'Import náhradních dílu > Import dílu {m} > Import náhradní díl pro {n}' for m, n in v))

    with open(output_csv_path, 'w', encoding='utf-8-sig') as fh:
        value = output.getvalue()
        fh.write(value)

        logger.info(f"Written {len(value) // 1024:,}KB to out.csv")


if __name__ == "__main__":
    init_logger(global_level=logging.DEBUG, main_level=logging.INFO)
    main()
