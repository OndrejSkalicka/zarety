import xml.etree.ElementTree as ET
from datetime import datetime

import requests
from openpyxl import Workbook

ATTRS = [
    'ITEM_ID',
    'ITEMGROUP_ID',
    'PRODUCTNAME',
    'PARAM[1]/PARAM_NAME',
    'PARAM[1]/VAL',
    'PARAM[2]/PARAM_NAME',
    'PARAM[2]/VAL',
]
URL = 'https://www.bertoo.cz/glami/export/products.xml'

response = requests.get(URL)
response.raise_for_status()
as_xml = response.text

root = ET.fromstring(as_xml)

wb = Workbook()
sheet = wb.active
sheet.title = 'GlamiXML'

sheet.append(ATTRS)

for child in root:
    row = list(el.text if el is not None else 'N/A' for el in (child.find(attr) for attr in ATTRS))
    sheet.append(row)
    print(row)

wb.save(filename='%s-%s.xlsx' % ('glami-xml-', datetime.now().strftime('%Y-%d-%m-%H-%M-%S')))
