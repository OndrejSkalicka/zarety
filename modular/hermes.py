from bs4 import BeautifulSoup

from zarety import load_urls, save_workbook, process_urls_simple, workbook_sheet_errors


def process(soup: BeautifulSoup):
    result = list(a.attrs['href'] for a in soup.select('.product_activeimages_image_div a'))
    if not result:
        return None

    return result


urls = load_urls('URL_SET.xlsx')
workbook, sheet, errors = workbook_sheet_errors('hermes', ['URL', 'Alt 1', 'Alt 2', 'Alt 3', 'Alt 4', 'Alt 5'])
process_urls_simple(urls, sheet, process, error_sheet=errors)
save_workbook(workbook)
