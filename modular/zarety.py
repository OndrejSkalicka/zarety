import csv
import traceback
from datetime import datetime, timedelta
from enum import Enum
from time import time
from typing import List, Tuple, Optional, Iterable

import requests
from bs4 import BeautifulSoup
from openpyxl import Workbook, load_workbook
from openpyxl.worksheet.worksheet import Worksheet


def log(s):
    print('%s: %s' % (date_format(datetime.now()), s))


def date_format(dt) -> str:
    return dt.strftime('%Y-%m-%d %H:%M:%S')


def load_urls(filename='urls.csv', limit: Optional[int] = None) -> List[str]:
    urls = []

    if filename.endswith('.csv'):

        with open(filename, newline='', mode='r', encoding='utf-8-sig') as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            for row in reader:
                if row:
                    urls.append(row[0])

    elif filename.endswith('.xlsx'):
        workbook = load_workbook(filename)
        worksheet = workbook.worksheets[0]
        for row in worksheet.iter_rows():
            if not row:
                continue

            value = str(row[0].value).strip()
            if value.startswith('https://'):
                urls.append(value)

    log('Loaded %d items from %s' % (len(urls), filename))

    if limit:
        return urls[:limit]

    return urls


def workbook_and_sheet(title: str, headers: Iterable[str]) -> Tuple[Workbook, Worksheet]:
    workbook = Workbook()
    sheet_main = workbook.active
    sheet_main.title = title

    for idx, title in enumerate(headers):
        sheet_main.cell(1, 1 + idx, title)

    return workbook, sheet_main


def workbook_sheet_errors(title: str, headers: Iterable[str]) -> Tuple[Workbook, Worksheet, Worksheet]:
    workbook = Workbook()
    sheet_main = workbook.active
    sheet_main.title = title

    for idx, title in enumerate(headers):
        sheet_main.cell(1, 1 + idx, title)

    sheet_error = workbook.create_sheet('Errors')
    for idx, title in enumerate(['URL', 'status', 'extra']):
        sheet_error.cell(1, 1 + idx, title)

    return workbook, sheet_main, sheet_error


def save_workbook(workbook: Workbook, prefix: Optional[str] = None):
    if prefix is None:
        prefix = workbook.sheetnames[0]

    workbook.save(filename='%s-%s.xlsx' % (prefix, datetime.now().strftime('%Y-%d-%m-%H-%M-%S')))


OK = 'OK'
SKIPPED = 'SKIPPED'
ERROR = 'ERROR'


class AnsiColors(Enum):
    # see https://en.wikipedia.org/wiki/ANSI_escape_code
    YELLOW = '\033[33m'
    BRIGHT_YELLOW = '\033[93m'
    DIM_ITALICS = '\033[90m\033[3m'
    BOLD = '\033[1m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    RESET = '\033[0m'

    def __str__(self):
        return self.value


def format_status(status: str, strf: Optional[str] = '%-7s') -> str:
    color = None
    if status == OK:
        color = AnsiColors.GREEN

    if status == SKIPPED:
        color = AnsiColors.BRIGHT_YELLOW

    if status == ERROR:
        color = AnsiColors.RED

    if not color:
        if strf:
            return strf % status
        return status

    if strf:
        status_formatted = strf % status
    else:
        status_formatted = status

    return ansi(color, status_formatted)


def ansi(color, text) -> str:
    return '%s%s%s' % (color, text, AnsiColors.RESET)


def add_error(error_sheet: Optional[Worksheet], url: str, status: str, extra: str = None):
    if error_sheet is None:
        return

    row = next_row(error_sheet)
    error_sheet.cell(row, 1, url)
    error_sheet.cell(row, 2, status)
    if extra:
        error_sheet.cell(row, 3, extra)


def next_row(sheet: Worksheet):
    if sheet.max_row > 1:
        return sheet.max_row + 1

    return len(list(sheet.iter_rows())) + 1


def process_urls_simple(urls: List[str], sheet: Worksheet, callback, error_sheet: Optional[Worksheet] = None):
    start_global = time()
    averages = []
    errors = []
    stats = {
        OK: 0,
        SKIPPED: 0,
        ERROR: 0,
    }

    for idx, url in enumerate(urls):
        status = OK
        start_item = time()
        try:
            log('Processing %s' % ansi(AnsiColors.DIM_ITALICS, url))

            response = requests.get(url)
            response.raise_for_status()
            soup = BeautifulSoup(response.text, 'html.parser')

            callback_result = callback(soup)
            if callback_result is None:
                status = SKIPPED
                add_error(error_sheet, url, status)
            else:
                row_data = [url] + list(callback_result)
                xlsx_row = next_row(sheet)

                for (col, val) in enumerate(row_data):
                    sheet.cell(xlsx_row, col + 1, val)

        except Exception as e:
            status = ERROR
            traceback.print_exc()
            log('Error processing product %s: %s' % (url, e))
            errors.append((url, e))
            add_error(error_sheet, url, status, '%s' % e)

        finally:
            stats[status] += 1

            item_time = time() - start_item
            averages.append(item_time)
            time_per_item = ((time() - start_global) / (idx + 1))
            eta = (len(urls) - idx - 1) * time_per_item
            floating_average = sum(averages[-10:]) / len(averages[-10:])
            log("[%s] Processed % 5d/%d (%.1f%%), avg time per page: %.2fs, 10x floating average: %.2fs, ETA %ds (%s)" % (
                format_status(status), idx + 1, len(urls), (idx + 1) / len(urls) * 100.0, time_per_item,
                floating_average, eta, date_format(datetime.now() + timedelta(seconds=eta))
            ))

    if errors:
        log("!!!!!!!!!!!!!!!!!!!!")
        log("There were %d errors" % len(errors))

        for url, error in errors:
            log('URL %s was not parsed: %s' % (url, error))

        log("!!!!!!!!!!!!!!!!!!!!")

    tot = sum(stats.values())
    for key in (OK, SKIPPED, ERROR):
        log('%s: % 5d/%d (%.2f%%)' % (format_status(key), stats[key], tot, 100.0 * (stats[key] / max(1, tot))))

    log('Total time: %ds' % (time() - start_global))
