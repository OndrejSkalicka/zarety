import re

from bs4 import BeautifulSoup

from zarety import load_urls, save_workbook, process_urls_simple, workbook_sheet_errors


def strip_whitespace(text: str) -> str:
    return re.sub(r'\s{2,}', ' ', text.replace('\n', ' ')).strip()


def process(soup: BeautifulSoup):
    title = strip_whitespace(soup.find('h1', class_='product__header__title').text)

    breadcrumbs = ' > '.join(strip_whitespace(e.text) for e in soup.find('ol', class_='breadcrumb').findAll('li')[:-1])

    return title, breadcrumbs


urls = load_urls('drmax.xlsx')
workbook, sheet, errors = workbook_sheet_errors('drmax', ['URL', 'Name', 'Breadcrumbs'])
process_urls_simple(urls, sheet, process, error_sheet=errors)
save_workbook(workbook)
