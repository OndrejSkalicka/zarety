import itertools
import logging.config

from openpyxl import load_workbook, Workbook

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    wb_s = load_workbook(filename='data.xlsx')
    ws_s = wb_s.worksheets[0]

    wb_d = Workbook()
    ws_d = wb_d.active
    ws_d.title = 'Vystup'

    header = None
    max_prefix = 0
    processed = 0
    output = []
    for row in list(ws_s.rows):
        if header is None:
            header = list(x.value for x in row)
            continue

        base = list(x.value for x in row[:4])
        codes = row[3].value.split('|')

        code_groups = {}
        for code in codes:
            prefix, suffix = code.split('-')
            prefix = int(prefix)

            if '#' in suffix:
                suffix, cost = suffix.split('#')
                cost = int(cost)
            else:
                cost = ''

            if prefix not in code_groups:
                code_groups[prefix] = []

            code_groups[prefix].append((prefix, suffix, cost))

        condensed = list(it[1] for it in sorted(code_groups.items(), key=lambda x: x[0]))
        all_combinations = list(itertools.product(*condensed))
        logger.debug(f"Found {len(all_combinations)} combinations for product id {base[0]} code {base[1]} .")

        for combination in all_combinations:
            extras = [''] * (2 * max(x[0] for x in combination))

            new_code = str(base[1])
            for prefix, suffix, cost in combination:
                max_prefix = max(max_prefix, prefix)
                extras[2 * prefix - 2] = f'{prefix}-{suffix}'
                extras[2 * prefix - 1] = cost
                new_code += f'/{prefix}-{suffix}'

            row = base + [new_code] + extras
            output.append(row)

        processed += 1
        logger.debug(f"Processed {processed:,} rows, output {len(output):,}.")

    header.append('NewCode')
    for i in range(max_prefix):
        header.append(f'Varianta {i + 1}')
        header.append(f'Priplatek {i + 1}')

    logger.info(f"About to write {len(output)} rows back to excel, hold your hat...")
    ws_d.append(header)
    for row in output:
        ws_d.append(row)
    wb_d.save(filename='result.xlsx')
    logger.info("It's over, it's done.")


if __name__ == "__main__":
    init_logger(logging.DEBUG)
    main()
