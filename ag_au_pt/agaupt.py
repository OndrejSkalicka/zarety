import argparse
import logging.config
from io import BytesIO
from typing import Iterable, Optional, Sequence

import requests
import xlsxwriter
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.cell import Cell
from openpyxl.cell.cell import TYPE_STRING, TYPE_NULL, TYPE_NUMERIC
from openpyxl.worksheet.worksheet import Worksheet

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='AGAU, the Gold and Silver golem.')
    parser.add_argument('--source-xlsx-url', type=str, required=True, help='URL input/source xlsx file.')
    parser.add_argument('--commodities-api-token', type=str, required=True, help='Token to commodities-api.com.')
    parser.add_argument('--output-xlsx-path', type=str, required=True, help='Path on local filesystem where to store results.')
    parser.add_argument('--currency', type=str, default='EUR', help='Currency of results. Default: EUR.')

    args = parser.parse_args()

    source_file_url = args.source_xlsx_url
    commodities_api_token = args.commodities_api_token
    output_file = args.output_xlsx_path
    currency = args.currency

    au_ag_pt_in_currency_resp = requests.get(f'https://commodities-api.com/api/latest?access_key={commodities_api_token}&base={currency}&symbols=XAU%2CXAG%2CXPT')
    au_ag_pt_in_currency_resp.raise_for_status()

    au_in_currency = 1.0 / au_ag_pt_in_currency_resp.json()['data']['rates']['XAU']
    ag_in_currency = 1.0 / au_ag_pt_in_currency_resp.json()['data']['rates']['XAG']
    pt_in_currency = 1.0 / au_ag_pt_in_currency_resp.json()['data']['rates']['XPT']

    logger.info("Fetched prices from goldapi.")

    logger.info(f"AU price {au_in_currency} {currency}.")
    logger.info(f"AG price {ag_in_currency} {currency}.")
    logger.info(f"PT price {pt_in_currency} {currency}.")

    file_response = requests.get(source_file_url, stream=True)
    file_response.raise_for_status()
    logger.info(f'Downloaded file {source_file_url}, response {file_response}, length {len(file_response.raw.data)}B.')

    wb_src: Workbook = load_workbook(filename=BytesIO(file_response.raw.data))
    ws_src: Worksheet = wb_src.worksheets[0]

    wb_new = xlsxwriter.Workbook(output_file)
    ws_new = wb_new.add_worksheet(ws_src.title)

    row: Sequence[Cell]

    header: Iterable[Cell] = next(ws_src.rows)
    name_idx: Optional[int] = None
    sn_idx: Optional[int] = None
    plu_idx: Optional[int] = None
    au_flag_idx: Optional[int] = None
    ag_flag_idx: Optional[int] = None
    pt_flag_idx: Optional[int] = None

    header_row = []
    for idx, c in enumerate(header):
        header_row.append(cell_to_scalar(c))
        if c.value == 'name':
            name_idx = idx

        if c.value == 'serialNumber':
            sn_idx = idx

        if c.value == 'plu':
            plu_idx = idx

        if c.value == 'zlatoFlagActive':
            au_flag_idx = idx

        if c.value == 'stribroFlagActive':
            ag_flag_idx = idx

        if c.value == 'platinaFlagActive':
            pt_flag_idx = idx

    header_row.append('price')
    header_row.append('filteringProperty:Vykupna-cena')
    ws_new.write_row(0, 0, header_row)

    assert name_idx is not None, 'Column "name" not in source sheet.'
    assert sn_idx is not None, 'Column "serialNumber" not in source sheet.'
    assert plu_idx is not None, 'Column "plu" not in source sheet.'
    assert au_flag_idx is not None, 'Column "zlatoFlagActive" not in source sheet.'
    assert ag_flag_idx is not None, 'Column "stribroFlagActive" not in source sheet.'
    assert pt_flag_idx is not None, 'Column "platinaFlagActive" not in source sheet.'

    cnt = 0
    row_num = 1
    for row in list(ws_src.rows)[1:]:
        assert len(row) == len(header_row) - 2
        name = row[name_idx]
        sn = as_float(row[sn_idx])
        plu = as_float(row[plu_idx])

        is_au = cell_to_bool(row[au_flag_idx])
        is_ag = cell_to_bool(row[ag_flag_idx])
        is_pt = cell_to_bool(row[pt_flag_idx])

        new_row = list(cell_to_scalar(c) for c in row)
        sn_value = None
        plu_value = None

        if is_au:
            assert not is_ag, "Both 'au' and 'ag' in single row."
            assert not is_pt, "Both 'au' and 'pt' in single row."

            if sn is not None:
                sn_value = round(sn * au_in_currency, 2)

            if plu is not None:
                plu_value = round(plu * au_in_currency, 2)

        if is_ag:
            assert not is_au, "Both 'ag' and 'au' in single row."
            assert not is_pt, "Both 'ag' and 'pt' in single row."

            if sn is not None:
                sn_value = round(sn * ag_in_currency, 2)

            if plu is not None:
                plu_value = round(plu * ag_in_currency, 2)

        if is_pt:
            assert not is_au, "Both 'pt' and 'au' in single row."
            assert not is_ag, "Both 'pt' and 'ag' in single row."

            if sn is not None:
                sn_value = round(sn * pt_in_currency, 2)

            if plu is not None:
                plu_value = round(plu * pt_in_currency, 2)

        new_row.append(sn_value)
        new_row.append(plu_value)

        ws_new.write_row(row_num, 0, new_row)
        row_num += 1

        cnt += 1
        if cnt % 50 == 0:
            logger.debug("Processed %d rows", cnt)

    logger.debug("Processed %d rows", cnt)

    wb_new.close()
    logger.info(f"Saved to {output_file}.")


def cell_to_scalar(cell: Cell) -> str:
    if cell.data_type == TYPE_STRING:
        return str(cell.value)

    if cell.data_type == TYPE_NULL:
        return ''

    raise ValueError("Illegal cell type %s" % cell.data_type)


def cell_to_bool(cell: Cell) -> bool:
    if cell.data_type == TYPE_STRING:
        return str(cell.value) == '1'

    if cell.data_type == TYPE_NUMERIC:
        return int(cell.value) == 1

    raise ValueError("Illegal cell type %s" % cell.data_type)


def as_float(cell: Cell) -> Optional[float]:
    if cell.data_type == TYPE_NULL:
        return None

    if cell.data_type != TYPE_STRING:
        logger.warning(f"Wrong data type for cell {cell}: {cell.data_type}")
        return None

    if cell.value == '':
        return None

    return float(cell.value.replace(',', '.'))


if __name__ == "__main__":
    init_logger(logging.DEBUG)
    main()
