import argparse
import csv
import io
import logging.config
from typing import Iterable

import requests
from openpyxl.cell import Cell
from openpyxl.reader.excel import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='XLSX to CSV converter')
    parser.add_argument('--source-xlsx-url', type=str, required=True, help='URL input/source xlsx file.')
    parser.add_argument('--output-csv-path', type=str, required=True, help='Path on local filesystem where to store results.')

    args = parser.parse_args()

    source_xlsx_url = args.source_xlsx_url
    output_csv_path = args.output_csv_path

    logger.info(f'Downloading {source_xlsx_url}.')

    file_response = requests.get(source_xlsx_url, stream=True, verify=False)
    file_response.raise_for_status()
    logger.info(f'Downloaded file {source_xlsx_url}, response {file_response}, length {len(file_response.raw.data)}B.')

    wb_src: Workbook = load_workbook(filename=io.BytesIO(file_response.raw.data))

    assert len(wb_src.worksheets) == 1, f'Expected to have exactly one sheet, got {", ".join(w.title for w in wb_src.worksheets)}'

    ws_src: Worksheet = wb_src.worksheets[0]

    with open(output_csv_path, 'w', encoding='utf-8') as fh:
        writer = csv.writer(fh, delimiter=';', quoting=csv.QUOTE_MINIMAL)

        for row in ws_src.rows:  # type: Iterable[Cell]
            writer.writerow(list(cell.value for cell in row))

    logger.info(f"Output written to {output_csv_path}")


if __name__ == "__main__":
    init_logger(global_level=logging.DEBUG, main_level=logging.INFO)
    main()
