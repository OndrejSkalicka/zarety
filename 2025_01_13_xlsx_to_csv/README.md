# XLSX to CSV converter

## Description

Converts a single-worksheet XLSX into a CSV.

Does not handle any excel dark magic. Just takes values of fields.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python xlsx_to_csv.py "--source-xlsx-url=https://ads-feed.s3.us-west-2.amazonaws.com/ads/XXX.xlsx" --output-csv-path=out.csv
```
