import requests
from bs4 import BeautifulSoup
import csv


urls = []

with open('urls.csv', newline='', mode='r', encoding='utf-8-sig') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    for row in reader:
        if row:
            urls.append(row[0])


result = []

for url in urls:
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')

    item = [url]

    divs = soup.find('div', class_='sklady').find_all('div')
    for div in divs:
        item.append(div.text)

    result.append(item)
    print('Processed %s' % url)

with open('result.csv', 'w', newline='', encoding='utf-8-sig') as csvfile:
    writer = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

    for row in result:
        writer.writerow(row)
