# Simple web crawler

## Usage

Requires Python 3.8+

Install dependencies:

```
pip install -r requirements.txt
```

update your `urls.csv` file and run

```
python crawler.py
```
