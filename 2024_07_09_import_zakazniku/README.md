# Import Zakazniku

## Description

Downloads a CSV export of customer-export, modifies it (moves value from column 'billingCompany' to 'remark') and imports into a Shoptet.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python import_zakazniku.py "--csv-url=https://123.myshoptet.com/export/customers.csv?partnerId=4&hash=xxx" --shop-url=https://123.myshoptet.com/admin/ --username=xxx --password=xxx
```
