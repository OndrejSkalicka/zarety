import argparse
import csv
import logging.config
import re
from io import StringIO
from typing import Tuple

import requests
from bs4 import BeautifulSoup

from logger_config import init_logger

logger = logging.getLogger(__name__)


# noinspection DuplicatedCode
def main():
    parser = argparse.ArgumentParser(description='Parses a modifies a CSV file for import-zakazniku use case')
    parser.add_argument('--csv-url', type=str, required=True, help='URL of the CSV catalogue')
    parser.add_argument('--shop-url', type=str, required=True, help='URL of admin, should end with .../admin/ .')
    parser.add_argument('--username', type=str, required=True, help='username.')
    parser.add_argument('--password', type=str, required=True, help='password.')

    args = parser.parse_args()

    csv_url = args.csv_url
    shop_url = args.shop_url
    username = args.username
    password = args.password

    assert shop_url.endswith('/admin/')

    login_url = f'{shop_url}login/'
    customer_import_url = f'{shop_url}import-zakazniku/'

    logger.info(f"Downloading {csv_url}")

    csv_response = requests.get(csv_url)
    csv_response.raise_for_status()

    reader = csv.DictReader(StringIO(csv_response.content.decode('utf-8-sig')), delimiter=";", quotechar='"')
    rows: list[dict[str, str]] = list(row for row in reader)  # type: ignore
    output = StringIO()

    writer = csv.DictWriter(output, list(rows[0].keys()), delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
    writer.writeheader()
    for row in rows:
        if row['billingCompany']:
            row['remark'] = row['billingCompany']
            row['billingCompany'] = ''
        writer.writerow(row)

    logger.info(f"Downloaded and converted CSV, {len(output.getvalue()):,}B. Proceeding to upload!")

    s = requests.Session()
    logger.info('Session started')

    # grab login page
    login_page = s.get(shop_url, allow_redirects=True)
    login_page.raise_for_status()

    # fetch csrf
    _, csrf = grab_both(login_page.text)

    logger.info(f'Current cookie jar: {s.cookies}')

    # log in
    login_result = s.post(
        url=login_url,
        data={
            'action': 'login',
            'email': username,
            'password': password,
            '__csrf__': csrf
        },
        allow_redirects=True,
        headers={
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:123.0) Gecko/20100101 Firefox/123.0",
        }
    )
    login_result.raise_for_status()

    logger.info(f'Logged in via {login_url}: {login_result}.')
    logger.info('Page title after login: %s' % BeautifulSoup(login_result.text, 'html.parser').find('title'))

    _, csrf = grab_both(login_result.text)

    upload_result = s.post(
        url=customer_import_url,
        files={
            'file': ('customers.csv', output.getvalue(), 'text/csv')
        },
        data={
            "__csrf__": csrf,
            "action": "import",
        },
        allow_redirects=True,
        headers={
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:123.0) Gecko/20100101 Firefox/123.0",
        }
    )
    upload_result.raise_for_status()

    logger.info(f'Uploaded file with result {upload_result}')
    logger.info('Page title after upload: %s' % BeautifulSoup(login_result.text, 'html.parser').find('title'))

    bs = BeautifulSoup(upload_result.text, 'html.parser')
    errors = [x.text for x in bs.findAll('div', {'class': 'systemMessage--error'})]
    if errors:
        logger.error(f"Found errors in response! {errors}")
        raise Exception

    success_messages = ';'.join(x.text.strip() for x in bs.findAll('div', {'class': 'systemMessage--success'}))

    logger.info(f'Success messages: {success_messages}')


# noinspection DuplicatedCode
def grab_both(text: str) -> Tuple[str, str]:
    title = grab_title(text)
    csrf = grab_csrf(text)

    logger.debug(f'Extracted CSRF {csrf} from page "{title}"')

    return title, csrf


def grab_title(text: str) -> str:
    return BeautifulSoup(text, 'html.parser').find('title').text


def grab_csrf(text: str) -> str:
    return re.findall(r'shoptet\.csrf\.token = "(.*?)"', text)[0]


if __name__ == "__main__":
    init_logger(global_level=logging.DEBUG, main_level=logging.INFO)
    main()
