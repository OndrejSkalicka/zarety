import argparse
import io
import logging.config
from sys import getsizeof

import paramiko
from openpyxl import load_workbook, Workbook
from openpyxl.worksheet.worksheet import Worksheet

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='One shot FTP files renamer.')
    parser.add_argument('--xlsx-path', type=str, required=True, help='Path on filesystem where XLSX with transformations is stored.')
    parser.add_argument('--sftp-host', type=str, required=True, help='Target SFTP host (port assumed :22).')
    parser.add_argument('--sftp-username', type=str, required=True, help='Target SFTP username.')
    parser.add_argument('--sftp-password', type=str, required=True, help='Target SFTP password.')
    parser.add_argument('--sftp-source-directory', type=str, required=True, help='Directory on SFTP where to read data.')
    parser.add_argument('--sftp-destination-directory', type=str, required=True, help='Directory on SFTP where to store results.')

    args = parser.parse_args()

    xlsx_path = args.xlsx_path
    sftp_host = args.sftp_host
    sftp_username = args.sftp_username
    sftp_password = args.sftp_password
    sftp_source_directory = args.sftp_source_directory
    sftp_destination_directory = args.sftp_destination_directory

    logger.info(f"Loading XLSX {xlsx_path}")
    wb: Workbook = load_workbook(filename=xlsx_path, data_only=True)
    ws: Worksheet = wb.worksheets[0]

    missing_files = []

    with paramiko.SSHClient() as ssh, io.BytesIO() as memfile:
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(sftp_host, port=22, username=sftp_username, password=sftp_password)
        sftp = ssh.open_sftp()
        try:
            sftp.chdir(sftp_destination_directory)
        except IOError:
            sftp.mkdir(sftp_destination_directory)
        sftp.chdir('/')

        logger.info(f"Connected to {sftp_host}, source dir in {sftp_source_directory}, destination dir {sftp_destination_directory}")

        for row in ws.iter_rows(min_row=2):

            if not row[0].value:
                continue

            from_name = str(row[0].value)
            to_name = str(row[1].value)

            file_from = sftp_source_directory + '/' + from_name
            file_to = sftp_destination_directory + '/' + to_name

            logger.debug(f"Trying to copy {file_from} to {file_to}")

            try:
                sftp.getfo(file_from, memfile)
                memfile.seek(0)
            except FileNotFoundError:
                logger.error(f"File {file_from} not found! Continue with next one.")
                missing_files.append(file_from)
                continue

            logger.debug(f'Downloaded {file_from}, size {getsizeof(memfile)}B')

            sftp.putfo(memfile, file_to)
            logger.info(f'Copied {file_from} to {file_to}')

    if missing_files:
        logger.error(f"Could not process those files: {missing_files}")


if __name__ == "__main__":
    init_logger(global_level=logging.DEBUG, main_level=logging.INFO)
    main()
