# Sosator

## Description

Simple tool to download (sosat) files that have very long timeout (on-the-fly-generation) and store them on filesystem.

## Usage

Requires Python 3.8+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python sosator.py --url=http://feed.jika.cz/jika_cz.php --username=tatarm@besta.cz --password=fubar --target=/tmp/xxx/soubor.xml
```

## Help

Call 

```
python sosator.py -h
```
