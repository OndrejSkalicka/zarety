import requests
from datetime import datetime
from requests.auth import HTTPBasicAuth
import argparse

parser = argparse.ArgumentParser(description='Sosator, sosing stuff for Zarety since 2021.')
parser.add_argument('--url', type=str, required=True, help='URL to download.')
parser.add_argument('--username', type=str, default=None, help='BASICAUTH username, if used.')
parser.add_argument('--password', type=str, default=None, help='BASICAUTH password, if used.')
parser.add_argument('--target', type=str, required=True,
                    help='Target file where to store result, relative or absolute path.')
parser.add_argument('--timeout', type=int, default=30, help='Timeout in minutes, default 30 minutes.')

args = parser.parse_args()

url = args.url
username = args.username
password = args.password
target_file = args.target
timeout_m = args.timeout
timeout_s = timeout_m * 60

print('Welcome to Sosator!')
print('    __')
print('  <(o )___')
print('   ( ._> /')
print('    `---\'')
print()

auth = None
if username and password:
    print('%s Auth enabled, username: %s, password: %s' % (datetime.now().isoformat(), username,
                                                           '*' * len(password)))
    auth = HTTPBasicAuth(username, password)
else:
    print('%s Auth DISABLED', datetime.now().isoformat())

print('%s Timeout: %d minutes' % (datetime.now().isoformat(), timeout_m))
print('%s Target URL: %s' % (datetime.now().isoformat(), url))
print('%s Target FILE: %s' % (datetime.now().isoformat(), target_file))
print('%s Initiating download...' % datetime.now().isoformat())

with open(target_file, 'wb') as f:
    with requests.get(url, auth=auth, timeout=timeout_s, stream=True) as r:
        r.raise_for_status()
        for chunk in r.iter_content(chunk_size=8192):
            f.write(chunk)

print('%s Download finished!' % datetime.now().isoformat())
