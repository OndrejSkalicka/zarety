# Duplicitni EAN

## Description

Parses a JSON export, finds duplicities on EAN and writes those to a CSV where the cheapest in a duplicity set is marked as VISIBLE, rest as HIDDEN.

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python duplicitni_ean.py "--json-url=https://feeds.mergado.com/autodily-na-skodu-cz-plain-json-XXXXXX.json" --output-csv-path=out.csv
```
