import argparse
import csv
import logging.config
import re
from collections import defaultdict

import requests

from logger_config import init_logger

logger = logging.getLogger(__name__)


# noinspection DuplicatedCode
def main():
    parser = argparse.ArgumentParser(description='Parses a JSON export, finds duplicities on EAN and writes those to a CSV where '
                                                 'the cheapest in a duplicity set is marked as VISIBLE, rest as HIDDEN')
    parser.add_argument('--json-url', type=str, required=True, help='URL of the JSON export')
    parser.add_argument('--output-csv-path', type=str, required=True, help='Path on local filesystem where to store results.')

    args = parser.parse_args()

    json_url = args.json_url
    output_csv_path = args.output_csv_path

    logger.info(f"Downloading {json_url}")

    json_response = requests.get(json_url)
    json_response.raise_for_status()

    products_json = json_response.json()
    products_by_ean = defaultdict(list)

    for product in products_json:
        ean = product.get('EAN', '')
        if not re.match(r'^\d{13}$', ean):
            logger.debug(f"Ignoring product with ean '{ean}' (code {product['CODE']}), does not match \d{13}")
            continue

        products_by_ean[ean].append(product)

    logger.info(f'Found {len(products_by_ean):,} different EANs in a total of {len(products_json):,} products.')

    logger.info(f'A total of {(sum(1 for p in products_by_ean.values() if len(p) > 1)):,} EANs have a duplicity')

    logger.info(f'Writing to {output_csv_path}')

    with open(output_csv_path, 'w', encoding='utf-8') as fw:
        writer = csv.writer(fw)
        writer.writerow(['CODE', 'EAN', 'PRICE_VAT', 'VISIBILITY'])

        for ean, products in products_by_ean.items():
            if len(products) < 2:
                continue
            products.sort(key=lambda p: (int(p['PRICE_VAT']), p['CODE']))

            first = True
            for product in products:
                writer.writerow([product['CODE'], product['EAN'], product['PRICE_VAT'], 'VISIBLE' if first else 'HIDDEN'])
                first = False

    logger.info('Finished successfully')


if __name__ == "__main__":
    init_logger(global_level=logging.DEBUG, main_level=logging.INFO)
    main()
