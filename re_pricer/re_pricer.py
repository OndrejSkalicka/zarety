import argparse
import csv
import logging.config
from io import StringIO

import requests

from logger_config import init_logger

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(description='RePricer, to change price from one to another (duh?).')
    parser.add_argument('--source-csv-url', type=str, required=True, help='URL input/source xlsx file.')
    parser.add_argument('--output-csv-path', type=str, required=True, help='Path on local filesystem where to store results.')

    args = parser.parse_args()

    source_file_url = args.source_csv_url
    output_file = args.output_csv_path

    logger.info(f"Downloading source CSV from {source_file_url}")

    response = requests.get(source_file_url)
    response.raise_for_status()
    response.encoding = 'cp1250'  # because fuck you...
    csv_text = response.text
    csv_io = StringIO(csv_text)

    reader = csv.reader(csv_io, delimiter=';')

    src_data = []
    for row in reader:
        src_data.append(row)

    base_prices = {}
    for row in src_data[1:]:
        if row[3] != '':
            base_prices[row[0]] = float(row[3].replace(',', '.'))

    logger.info(f"Parsed {len(base_prices)} prices")

    new_data = []
    new_header = []

    for header in src_data[0]:
        if header.startswith('setItem'):
            new_header.append(header)
            new_header.append(header.replace('setItem', 'setItemprice'))

        else:
            new_header.append(header)
    new_data.append(new_header)

    logger.info(f"New header {new_header}")

    for row in src_data[1:]:
        new_row = []

        header: str
        for i, header in enumerate(src_data[0]):
            row_value = row[i] if i < len(row) else ''

            if header.startswith('setItem'):
                new_row.append(row_value)

                if ';' in row_value:
                    code, quantity = row_value.split(';')
                    quantity = float(quantity)

                    if code in base_prices:
                        new_row.append(quantity * base_prices[code])
                    else:
                        new_row.append('ERROR: PYCA TOHLE NEMUZE NASTAT!! --ZARETY, 06/2022!')
                else:
                    new_row.append(row_value)


            else:
                new_row.append(row_value)

        new_data.append(new_row)

    logger.info(f"Processed {len(new_data)} rows")

    with open(output_file, "w", newline="", encoding='utf-8') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerows(new_data)

    logger.info(f"Saved result to {output_file}")


if __name__ == "__main__":
    init_logger(logging.DEBUG)
    main()
