# Re-Pricer

## Description

Script that downloads a CSV from internet, processes some math in between rows (calculates price of item based on a price of related item) and writes to output CSV.

## Usage

Requires Python 3.8+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python re_pricer.py --source-csv-url=https://521880.myshoptet.com/export/products.csv?patternId=1&hash=da0b51fd7103d2d453fe110978f2ab4c611f4cbd6956dfdd9e0bb08c1008aa1f --output-csv-path=path/to/output.csv
```

First parameter should be provided by Zarety. Second is where result will be stored (must be writable). 
