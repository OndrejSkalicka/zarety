import argparse
import re

import requests


def main():
    parser = argparse.ArgumentParser(description='Calculates KAS hashrate')
    parser.add_argument('--mh-s', type=int, required=True, help='Mh/s')

    args = parser.parse_args()

    mh_s = args.mh_s

    response = requests.get('https://widgets.hashrate.no/coinsCalculator/KAS')
    response.raise_for_status()

    m = re.findall(r'estimateUSD24 = (.*?);', response.text)

    assert m, "No match for `estimateUSD24 = (.*?);`"
    print(float(m[0]) * mh_s)


if __name__ == "__main__":
    main()
