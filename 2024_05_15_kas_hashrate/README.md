# KAS Hashrate

## Description

Simple calculator, extracts hashrate value from a hashrate.no widget and multiplies by input

## Usage

Requires Python 3.10+

Install dependencies:

```
pip install -r requirements.txt
```

Call using

```
python kas_hashrate.py --mh-s=1000000  
```
