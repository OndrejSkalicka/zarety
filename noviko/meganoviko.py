import argparse
import base64
import concurrent.futures
import json
import logging
import logging.config
import os.path
import sys
import time
from copy import deepcopy
from logging.handlers import RotatingFileHandler
from typing import Optional, List, Dict

import dicttoxml
import requests
import urllib3
from requests.auth import HTTPBasicAuth

logger = logging.getLogger(__name__)


def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))


class ColorFormatter(logging.Formatter):
    grey = "\033[37;20m"
    yellow = "\033[33;20m"
    red = "\033[31;20m"
    bold_red = "\033[31;1m"
    reset = "\033[0m"
    format_str = "%(asctime)s.%(msecs)03d - %(levelname)-8s - %(message)s (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.DEBUG: grey + format_str + reset,
        logging.INFO: format_str,
        logging.WARNING: yellow + format_str + reset,
        logging.ERROR: red + format_str + reset,
        logging.CRITICAL: bold_red + format_str + reset
    }

    def format(self, record: logging.LogRecord) -> str:
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt, datefmt='%Y-%m-%dT%H:%M:%S%z')
        return formatter.format(record)


def init_logger(level: int = logging.DEBUG) -> None:
    console = logging.StreamHandler(stream=sys.stdout)
    console.setLevel(level)
    console.setFormatter(ColorFormatter())

    file = RotatingFileHandler('log.txt', 'a', 10000000, 10)
    file.setLevel(level)
    file.setFormatter(ColorFormatter())

    logging.getLogger('').addHandler(console)
    logging.getLogger('').addHandler(file)
    logging.getLogger('').setLevel(logging.WARN)
    logger.setLevel(level)


def download_products_to_file(products_url: str, products_file: str, auth: Optional[HTTPBasicAuth], ignore_ssl: bool) -> str:
    logger.info(f'Initiating download of {products_url}...')

    total_bytes = 0
    last_bytes_log = 0
    log_first_bytes = True
    LAST_BYTES_LOG_INCREMENT = 1024 ** 2
    all_bytes = bytes()

    with requests.get(products_url, auth=auth, stream=True, verify=not ignore_ssl, headers={'Accept': 'application/json'}) as r:
        r.raise_for_status()

        with open(products_file, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                if log_first_bytes:
                    log_first_bytes = False
                    logger.info("Got first bytes of data!")

                total_bytes += len(chunk)
                all_bytes += chunk

                if total_bytes > last_bytes_log + LAST_BYTES_LOG_INCREMENT:
                    logger.info("Downloaded %5.1f MB" % (total_bytes / 1024 / 1024))
                    last_bytes_log += LAST_BYTES_LOG_INCREMENT

                f.write(chunk)

    logger.info("Downloaded total of %d B" % total_bytes)
    logger.info('Download finished!')

    return all_bytes.decode('UTF-8')


def product_photo_description_dict(photo_url_template: str, product_id: int, auth: Optional[HTTPBasicAuth], ignore_ssl: bool) -> dict:
    photo_url = photo_url_template % product_id

    response = requests.get(photo_url, auth=auth, verify=not ignore_ssl, headers={'Accept': 'application/json'})
    response.raise_for_status()
    return response.json()


def process_product(product: dict, photo_url_template: str, auth: Optional[HTTPBasicAuth], ignore_ssl: bool, out_directory: str,
                    output_url_template: str, perf_stats: Dict[str, List[int]]) -> Optional[dict]:
    start_ns = time.time_ns()
    product = deepcopy(product)
    perf_stats['copy'].append((time.time_ns() - start_ns) // 1_000_000)

    product_id = product['productInternalId']

    logger.debug(f"Processing product {product_id}.")

    try:
        start_ns = time.time_ns()
        photo_description = product_photo_description_dict(photo_url_template, product_id, auth, ignore_ssl)
        logger.debug(f"Downloaded p/d for {product_id}.")
        perf_stats['http'].append((time.time_ns() - start_ns) // 1_000_000)
    except requests.exceptions.HTTPError as e:
        logger.error(f"Got an error when accessing {product_id:10}: {e}")
        # CR: even if product cannot be retrieved, return it, but without photo/description
        return product

    if 'foto' in photo_description:
        start_ns = time.time_ns()
        decoded = base64.b64decode(photo_description['foto'])
        perf_stats['decode'].append((time.time_ns() - start_ns) // 1_000_000)

        photo_path = f'{out_directory}/photo.{product_id}.jpg'

        start_ns = time.time_ns()
        with open(photo_path, 'wb') as f:
            f.write(decoded)
        perf_stats['save'].append((time.time_ns() - start_ns) // 1_000_000)

        logger.debug("Saved %d bytes to %s" % (len(decoded), photo_path))
        product['photo_url'] = output_url_template % product_id
    else:
        product['photo_url'] = ''

    if 'info' in photo_description:
        product['description'] = photo_description['info']
    else:
        product['description'] = ''

    return product


def main():
    sys.excepthook = handle_exception

    parser = argparse.ArgumentParser(description='Noviko tool to parse cenik.xml.')
    parser.add_argument('--base-url', type=str, required=True, help='Noviko base URL, not ending with "/".')
    parser.add_argument('--username', type=str, required=True, help='BASICAUTH username.')
    parser.add_argument('--password', type=str, required=True, help='BASICAUTH password.')
    parser.add_argument('--target-directory', type=str, required=True, help='Target file where to store result, relative or absolute path.')
    parser.add_argument('--ignore-ssl', action='store_const', const=True, default=False, help='Whether to skip SSL verification.')

    args = parser.parse_args()

    noviko_base_url = args.base_url
    username = args.username
    password = args.password
    out_directory = args.target_directory
    ignore_ssl = args.ignore_ssl

    products_url = f'{noviko_base_url}/restapi/b2b/zbozi'
    photo_url_template = f'{noviko_base_url}/restapi/b2b/zbozi/%d/foto'
    products_file = f'{out_directory}/zbozi.json'
    output_url_template = 'http://zaretyho.server.com/noviko/photo.%d.jpg'
    limit: int = 100_000
    threads: int = 1
    perf_stats: Dict[str, List[int]] = {
        'copy': [],
        'http': [],
        'decode': [],
        'save': [],
    }

    if not os.path.exists(out_directory):
        os.makedirs(out_directory)

    auth: Optional[HTTPBasicAuth] = None
    if username and password:
        logger.info('Auth enabled, username: %s, password: %s' % (username, '*' * len(password)))
        auth = HTTPBasicAuth(username, password)
    else:
        logger.info('Auth DISABLED')

    if ignore_ssl:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    logger.info('Products URL: %s' % products_url)
    logger.info('Noviko base URL: %s' % noviko_base_url)
    logger.info('Ignore SSL: %s' % ignore_ssl)

    products_string = download_products_to_file(products_url, products_file, auth, ignore_ssl)

    products_json = json.loads(products_string)

    result = []
    start_time = time.time()

    if threads > 1:
        with concurrent.futures.ThreadPoolExecutor(max_workers=threads) as executor:

            tasks = []

            counter = 0
            for product in products_json['product']:
                counter += 1
                if 0 < limit < counter:
                    break

                tasks.append(executor.submit(process_product,
                                             product=product, photo_url_template=photo_url_template, auth=auth, ignore_ssl=ignore_ssl,
                                             out_directory=out_directory, output_url_template=output_url_template, perf_stats=perf_stats))

            counter = 0
            for task in concurrent.futures.as_completed(tasks):
                task_result = task.result()
                if task_result is not None:
                    result.append(task_result)
                counter += 1

                if counter % 50 == 0:
                    logger.info(f"Processed {counter:6,}/{len(products_json['product']):6,} items. Active TPS: {counter / (time.time() - start_time):.2f}.")
                    for k, v in perf_stats.items():
                        logger.info('Perf stats for %s: %dms (%dx)' % (k, sum(v) // len(v), len(v)))
    else:
        counter = 0
        for product in products_json['product']:
            counter += 1
            if 0 < limit < counter:
                break

            task_result = process_product(product, photo_url_template, auth, ignore_ssl, out_directory, output_url_template, perf_stats)

            if task_result is not None:
                result.append(task_result)

            if counter % 50 == 0:
                logger.info(f"Processed {counter:6,}/{len(products_json['product']):6,} items. Active TPS: {counter / (time.time() - start_time):.2f}.")
                for k, v in perf_stats.items():
                    logger.info('Perf stats for %s: %dms (%dx)' % (k, sum(v) // max(1, len(v)), len(v)))

    logger.info(f"Processed {len(result):,} items.")

    with open(f'{out_directory}/result.json', 'w') as f:
        f.write(json.dumps(result))

    logger.info('Written output to {out_directory}/result.json')

    with open(f'{out_directory}/result.xml', 'wb') as f:
        f.write(dicttoxml.dicttoxml(result, custom_root='products', attr_type=False))

    logger.info('Written output to {out_directory}/result.xml')


if __name__ == "__main__":
    init_logger(logging.INFO)
    main()
