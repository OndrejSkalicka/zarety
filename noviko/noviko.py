import argparse
import logging.config
import sys

import requests
import urllib3
from requests.auth import HTTPBasicAuth


def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logging.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))


sys.excepthook = handle_exception

logging.config.fileConfig('logging.conf')

parser = argparse.ArgumentParser(description='Noviko tool to parse cenik.xml.')
parser.add_argument('--url', type=str, required=True, help='URL to download.')
parser.add_argument('--username', type=str, required=True, help='BASICAUTH username.')
parser.add_argument('--password', type=str, required=True, help='BASICAUTH password.')
parser.add_argument('--target', type=str, required=True,
                    help='Target file where to store result, relative or absolute path.')
parser.add_argument('--ignore-ssl', action='store_const', const=True, default=False,
                    help='Whether to skip SSL verification.')

args = parser.parse_args()

url = args.url
username = args.username
password = args.password
target_file = args.target
ignore_ssl = args.ignore_ssl

logging.info(r'''Welcome to Noviko downloader.

Also, say Hi to Santa!
   ,--.
  ()   \
   /    \
 _/______\_
(__________)
(/  @  @  \)
(`._,()._,')
(  `-'`-'  )
 \        /
  \,,,,,,/
  ''')

auth = None
if username and password:
    logging.info('Auth enabled, username: %s, password: %s' % (username, '*' * len(password)))
    auth = HTTPBasicAuth(username, password)
else:
    logging.info('Auth DISABLED')

if ignore_ssl:
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

logging.info('Target URL: %s' % url)
logging.info('Target FILE: %s' % target_file)
logging.info('Ignore SSL: %s' % ignore_ssl)
logging.info('Initiating download...')

total_bytes = 0
last_bytes_log = 0
log_first_bytes = True
LAST_BYTES_LOG_INCREMENT = 1024 ** 2
with requests.get(url, auth=auth, stream=True, verify=not ignore_ssl) as r:
    r.raise_for_status()

    with open(target_file, 'wb') as f:
        for chunk in r.iter_content(chunk_size=8192):
            if log_first_bytes:
                log_first_bytes = False
                logging.info("Got first bytes of data!")

            total_bytes += len(chunk)

            if total_bytes > last_bytes_log + LAST_BYTES_LOG_INCREMENT:
                logging.info("Downloaded %5.1f MB" % (total_bytes / 1024 / 1024))
                last_bytes_log += LAST_BYTES_LOG_INCREMENT

            f.write(chunk)

logging.info("Downloaded total of %d B" % total_bytes)
logging.info('Download finished!')
